<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/2/27 9:45
// +----------------------------------------------------------------------
// | Readme: 工厂类
// +----------------------------------------------------------------------

namespace Wkidt\Redis;

use app\common\exception\Exception;
use think\Config;

class Factory
{
    /**
     * 对象暂存器
     * @var array
     */
    protected static $object = [];

    /**
     * 创建redis对象
     * @param $config
     * @return \Redis
     * @throws Exception
     */
    public static function createRedis($config = [])
    {
        if (!extension_loaded('redis')) {
            throw new Exception('Redis extension not loaded.');
        }

        /**
         * 默认配置
         *
         */
        $defaultConfig = [
            'host'              => 'localhost',
            'port'              => 6379,
            'password'          => '',
            'database'          => 0,
            'persistent'        => false,
            'timeout'           => false
        ];

        $config = array_merge($defaultConfig, Config::get('redis'), $config);
        $key = md5(serialize($config));

        if (isset(self::$object[$key])) {
            return self::$object[$key];
        } else {
            $redis = new \Redis();
            $func = $config['persistent'] ? 'pconnect' : 'connect';
            $config['timeout'] === false ?
                $redis->$func($config['host'], $config['port']) :
                $redis->$func($config['host'], $config['port'], $config['timeout']);

            // 是否验证
            if (!is_null($config['password'])) {
                $redis->auth($config['password']);
            }

            // 选择数据库
            if (!is_null($config['database'])) {
                $redis->select($config['database']);
            }

            // 序列化
            $redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_PHP);
            self::$object[$key] = $redis;
            return self::$object[$key];
        }
    }

}
