<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/7/4 0004 16:36
// +----------------------------------------------------------------------
// | Readme: RSA加密类
// +----------------------------------------------------------------------

namespace Wkidt\Rsa;

class Rsa
{

    /**
     * 错误信息
     * @var string
     */
    private $error = '';

    /**
     * 公钥
     * @var string
     */
    private $pubKey = '';

    /**
     * 私钥
     * @var string
     */
    private $priKey = '';

    /**
     * 构造函数
     * @param string $key 公钥（验签和加密时传入）
     */
    public function __construct($key)
    {
        $this->parseKey($key);
    }

    /**
     * 获取错误信息
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * 处理秘钥
     * @param $key
     */
    private function parseKey($key)
    {
        $this->parsePriKey($key);
        $this->parsePubKey($key);
    }

    /**
     * 处理公钥
     * @param $pubKey
     */
    private function parsePubKey($pubKey)
    {
        if (!empty($pubKey)) {
            if ('-' != $pubKey[0]) {
                $pubKey = str_replace(PHP_EOL, '', $pubKey);
                $pubKey = chunk_split($pubKey, 64, PHP_EOL);
                $pubKey = "-----BEGIN PUBLIC KEY-----" . PHP_EOL . $pubKey . "-----END PUBLIC KEY-----";
            }
            $this->pubKey = openssl_pkey_get_public($pubKey);
        }
    }

    /**
     * 处理私钥
     * @param $priKey
     */
    private function parsePriKey($priKey)
    {
        if (!empty($priKey)) {
            if ('-' != $priKey[0]) {
                $priKey = str_replace(PHP_EOL, '', $priKey);
                $priKey = chunk_split($priKey, 64, PHP_EOL);
                $priKey = "-----BEGIN RSA PRIVATE KEY-----" . PHP_EOL . $priKey . "-----END RSA PRIVATE KEY-----";
            }
            $this->priKey = openssl_pkey_get_private($priKey);
        }
    }

    /**
     * 私钥加密
     * @param $data
     * @param string $code
     * @return string
     */
    public function priEncrypt($data, $code = 'base64')
    {
        $encrypted = '';
        openssl_private_encrypt($data, $encrypted, $this->priKey);
        openssl_free_key($this->priKey);
        return $this->_encode($encrypted, $code);
    }

    /**
     * 私钥解密
     * @param $data
     * @param $code
     * @return string
     */
    public function priDecrypt($data, $code = 'base64')
    {
        $data = $this->_decode($data, $code);
        $decrypted = '';
        openssl_private_decrypt($data, $decrypted, $this->priKey);
        openssl_free_key($this->priKey);
        return $decrypted;
    }

    /**
     * 私钥签名
     * @param $data
     * @param $code
     * @return string
     */
    public function priSign($data, $code = 'base64')
    {
        openssl_sign($data, $signMsg, $this->priKey, OPENSSL_ALGO_SHA1);
        openssl_free_key($this->priKey);
        return $this->_encode($signMsg, $code);
    }

    /**
     * 公钥验签
     * @param $data
     * @param $sign
     * @return bool
     */
    public function pubVerify($data, $sign)
    {
        $isOk = openssl_verify($data, base64_decode($sign), $this->pubKey, OPENSSL_ALGO_SHA1);
        openssl_free_key($this->pubKey);
        return $isOk == 1 ? true : false;
    }

    /**
     * 公钥加密
     * @param $data
     * @param string $code
     * @return string
     */
    public function pubEncrypt($data, $code = 'base64')
    {
        $encrypted = '';
        openssl_public_encrypt($data, $encrypted, $this->pubKey);
        openssl_free_key($this->pubKey);
        return $this->_encode($encrypted, $code);
    }

    /**
     * 公钥解密
     * @param $data
     * @param $code
     * @return string
     */
    public function pubDecrypt($data, $code = 'base64')
    {
        $data = $this->_decode($data, $code);
        $decrypted = '';
        openssl_public_decrypt($data, $decrypted, $this->pubKey);
        openssl_free_key($this->pubKey);
        return $decrypted;
    }

    /**
     * 编码
     * @param $data
     * @param $code
     * @return string
     */
    private function _encode($data, $code)
    {
        switch (strtolower($code)) {
            case 'base64':
                $data = base64_encode('' . $data);
                break;
            case 'hex':
                $data = bin2hex($data);
                break;
            case 'bin':
            default:
        }
        return $data;
    }

    /**
     * 解码
     * @param $data
     * @param $code
     * @return string
     */
    private function _decode($data, $code)
    {
        switch (strtolower($code)) {
            case 'base64':
                $data = base64_decode($data);
                break;
            case 'hex':
                $data = $this->_hex2bin($data);
                break;
            case 'bin':
            default:
        }
        return $data;
    }

    /**
     * 解码
     * @param bool|false $hex
     * @return bool|string
     */
    private function _hex2bin($hex = false)
    {
        $ret = $hex !== false && preg_match('/^[0-9a-fA-F]+$/i', $hex) ? pack("H*", $hex) : false;
        return $ret;
    }
}
