<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/6/7 0007 11:42
// +----------------------------------------------------------------------
// | Readme: 短信工具类
// +----------------------------------------------------------------------

namespace Wkidt\VCode;

use think\Cache;
use Wkidt\think5\exception\Exception;
use think\Config;
use Wkidt\VCode\Sender\Sender;

class VCode
{
    const RISK_HIGH = 3;
    const RISK_MEDIUM = 2;
    const RISK_LOW = 1;

    /**
     * 实例缓存
     *
     * @var array
     */
    protected static $map = [];

    /**
     * 获取短信验证码发送类实例
     *
     * @param array $config
     * @return Sender
     * @throws Exception
     */
    public static function createSmsVCode($config = [])
    {
        $config = array_merge(Config::get('sms_code'), $config);
        $key = md5('sms' . serialize($config));
        if (isset(self::$map[$key])) {
            return self::$map[$key];
        } else {
            $type = isset($config['type']) ? $config['type'] : '';
            $class = "Wkidt\\VCode\\Sender\\{$type}";

            if (class_exists($class)) {
                self::$map[$key] = new $class($config);
                return self::$map[$key];
            } else {
                throw new Exception('短信发送驱动不存在');
            }
        }
    }

    /**
     * 获取邮件发送类实例
     *
     * @param array $config
     * @throws Exception
     * @return Sender
     */
    public static function createEmailVCode($config = [])
    {

        throw new Exception('邮件发送驱动不存在');
    }

    /**
     * 获取语音发送类实例
     *
     * @param array $config
     * @throws Exception
     * @return Sender
     */
    public static function createVoiceVCode($config = [])
    {

        throw new Exception('语音发送驱动不存在');
    }

    /**
     * 获取验证码受攻击的风险等级 1 2 3
     *
     * @return int
     */
    public static function riskLevel()
    {
        if (false == $level = Cache::get('vcode_risk_level')) {
            $count = LogModel::instance()->where('create_time', 'gt', (time() - 10 * 60))->count();
            if ($count <= 10) {
                $level = self::RISK_LOW;
            } elseif ($count <= 20) {
                $level = self::RISK_MEDIUM;
            } else {
                $level = self::RISK_HIGH;
            }
            Cache::set('vcode_risk_level', $level, 180);
            return $level;
        } else {
            return $level;
        }
    }
}
