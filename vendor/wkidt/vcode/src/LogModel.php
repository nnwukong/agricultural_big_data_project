<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/6/14 11:48
// +----------------------------------------------------------------------
// | Readme: 验证码记录模型
// +----------------------------------------------------------------------

namespace Wkidt\VCode;

use Wkidt\think5\model\Model;

class LogModel extends Model
{

    /**
     * 数据表
     *
     * @var string
     */
    protected $name = 'vendor_code_log';

    /**
     * 类型
     *
     * @var array
     */
    protected $type = [
        'create_time' => 'int'
    ];
}

