<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/6/14 14:29
// +----------------------------------------------------------------------
// | Readme: 消息发送者
// +----------------------------------------------------------------------

namespace Wkidt\VCode\Sender;

use Wkidt\think5\exception\Exception;
use think\Request;
use Wkidt\VCode\LogModel;

abstract class Sender
{
    /**
     * 类型
     *
     * @var string
     */
    protected $type = '';

    /**
     * 验证码是否新生成的
     *
     * @var bool
     */
    protected $isNew = false;

    /**
     * 配置
     *
     * @var array
     */
    protected $config = [
        'valid_period' => 0
    ];

    /**
     * 发送验证码
     *
     * @param $target
     * @return mixed
     */
    abstract public function send($target);

    /**
     * 保存验证码
     *
     * @param $target
     * @param $code
     * @return bool
     */
    protected function saveCode($target, $code)
    {
        if ($this->isNew) {
            $logModel = new LogModel();
            $data = [
                'target' => $target,
                'type' => $this->type,
                'code' => $code,
                'create_time' => time(),
                'create_ip' => Request::instance()->ip()
            ];
            return $logModel->save($data);
        } else {
            return true;
        }
    }

    /**
     * 验证验证码
     *
     * @param $target
     * @param $code
     * @return bool
     * @throws Exception
     */
    public function verify($target, $code)
    {
        $logModel = new LogModel();
        $info = $logModel->where('target', $target)
            ->order('id desc')
            ->find();
        if (is_null($info)) {
            throw new Exception('请先获取验证码');
        } elseif ('' == $code) {
            throw new Exception('请输入验证码');
        } elseif ($info->getAttr('create_time') + $this->config['valid_period'] > time() && $info['verify_count'] <= 5 && '0' == $info['status']) {
            if ($info['code'] == $code) {
                $info->status = 1;
                $info->save();
                return true;
            } else {
                $info->verify_count++;
                $info->save();
                throw new Exception('验证码错误');
            }
        } else {
            throw new Exception('验证码已过期');
        }
    }

    /**
     * 获取验证码
     *
     * @param $target
     * @return string
     */
    protected function getCode($target)
    {
        $logModel = new LogModel();
        $info = $logModel->where('target', $target)
            ->order('id desc')
            ->find();

        if (!is_null($info) && ($info['create_time'] + $this->config['valid_period'] > time()) && '0' == $info['status']) {
            return $info['code'];
        } else {
            $this->isNew = true;
            return (string)rand(100000, 999999);
        }
    }
}

