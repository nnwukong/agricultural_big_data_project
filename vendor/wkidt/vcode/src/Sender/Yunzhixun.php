<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/6/7 0007 11:42
// +----------------------------------------------------------------------
// | Readme: 云之迅短信类
// +----------------------------------------------------------------------

namespace Wkidt\VCode\Sender;

use Wkidt\Base\Exception\Exception;
use Wkidt\Base\Exception\RuntimeException;
use Wkidt\VCode\Interfaces\SenderInterface;
use Wkidt\Curl\Curl;

class Yunzhixun implements SenderInterface{

    /**
     * 模板id
     * @var string
     */
    protected $templateId = '22962';

    /**
     * @var string
     * 开发者账号ID。由32个英文字母和阿拉伯数字组成的开发者账号唯一标识符。
     */
	protected $accountSid = '6c28a7a448e3e1c9335d42fa961ef738';

    /**
     * @var string
     * 开发者账号TOKEN
     */
	protected $token = '0efe4c7add14d9df783eae0cc15e68e6';

    /**
     * API请求地址
     */
    protected $url = "http://www.ucpaas.com/maap/sms/code";

    /**
     * @var string
     * 时间戳
     */
    private $timestamp;

    /**
     * @var string
     * 时间戳
     */
	private $appId = 'b04d9c4766904faf8f621017468ba778';

    /**
     * 构造方法
     */
    public function  __construct() {
        $this->timestamp = date("YmdHis").substr(microtime(true), -3);
    }

    /**
     * @return string
     * 验证参数,URL后必须带有sig参数，sig= MD5（账户Id + 账户授权令牌 + 时间戳，共32位）(注:转成大写)
     */
    private function getSigParameter()
    {
        $sig = $this->accountSid.$this->timestamp.$this->token;
        return md5($sig);
    }

    /**
     * 发送短信
     * @param $phone
     * @param $code
     * @param array $params
     * @return bool
     * @throws RuntimeException
     */
    public function send($phone, $code, $params=[]){
        //合成参数
        $data = array(
            'sid'=>$this->accountSid,
            'appId'=>$this->appId,
            'time'=>$this->timestamp,
            'templateId'=>$this->templateId,
            'to'=>$phone,
            'param'=>$code,
            'sign'=>$this->getSigParameter(),
        );

        //发送请求并处理返回
        $curl = new Curl();
        $res = $curl->post($this->url, $data);
        if( empty($res) ){
            throw new RuntimeException('接口错误');
        }else{
            $res = json_decode($res, true);
            if( $res['resp']['respCode']=='000000' ){
                return true;
            }else{
                throw new RuntimeException( $res['resp']['respMsg']?:'发送失败' );
            }
        }
    }


}