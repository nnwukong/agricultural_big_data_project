<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/4/7 16:00
// +----------------------------------------------------------------------
// | Readme: 创蓝短信驱动
// +----------------------------------------------------------------------

namespace Wkidt\VCode\Sender;

use Wkidt\Base\Exception\Exception;
use Wkidt\Base\Exception\RuntimeException;
use Wkidt\VCode\Interfaces\SenderInterface;
use Wkidt\Curl\Curl;

class Chuanglan implements SenderInterface
{
    /**
     * 短信内容
     * @var string
     */
    protected $content = '您的验证码是{0}，请在30分钟内完成验证码，如非本人操作请忽略！';

    /**
     * 语音验证码账户
     * @var string
     */
    protected $vAccount = '';

    /**
     * 语音验证码密码
     * @var string
     */
    protected $vPassword = '';

    /**
     * 验证码短信账户
     * @var string
     */
    protected $account = 'VIP8JISU';

    /**
     * 验证码短信密码
     * @var string
     */
    protected $password = 'Aa666666';

    /**
     * 借款地址
     * @var string
     */
    protected $url = 'http://222.73.117.158/msg/HttpBatchSendSM';

    /**
     * 发送短信
     * @param $phone
     * @param $code
     * @param array $params
     * @return bool
     * @throws RuntimeException
     */
    public function send($phone, $code, $params=[]){
        //合成参数
        $postArr = array (
            'account'       => $this->account,
            'pswd'          => $this->password,
            'msg'           => str_replace('{0}', $code, $this->content),
            'mobile'        => $phone,
            'needstatus'    => true,
            'product'       => '',
            'extno'         => ''
        );

        //发送请求并处理返回
        $curl = new Curl();
        $res = $curl->post($this->url, $postArr);
        if( empty($res) ){
            throw new RuntimeException('接口错误');
        }else{
            $res = explode(',', $res);
            if( '0'==$res[1] ){
                return true;
            }else{
                throw new RuntimeException('发送失败');
            }
        }
    }

}