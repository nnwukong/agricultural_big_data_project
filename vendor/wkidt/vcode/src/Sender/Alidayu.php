<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/6/14 14:42
// +----------------------------------------------------------------------
// | Readme: 阿里大于
// +----------------------------------------------------------------------

namespace Wkidt\VCode\Sender;

use AliyunMNS\Client;
use AliyunMNS\Exception\MnsException;
use AliyunMNS\Model\BatchSmsAttributes;
use AliyunMNS\Model\MessageAttributes;
use AliyunMNS\Requests\PublishMessageRequest;
use app\common\exception\Exception;

include_once __DIR__ . '/php_sdk/mns-autoloader.php';
date_default_timezone_set('Asia/Shanghai');

class Alidayu extends Sender
{

    /**
     * 类型
     *
     * @var string
     */
    protected $type = 'sms';

    /**
     * 配置
     *
     * @var array
     */
    protected $config = [
        'access_id' => '',
        'access_key' => '',
        'endpoint' => '',
        'sign' => '',
        'template_code' => '',
    ];

    /**
     * 初始化
     *
     * Alidayu constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->config = array_merge($this->config, $config);
    }


    /**
     * 发送验证码
     *
     * @param $target
     * @return bool
     */
    public function send($target)
    {
        $code = $this->getCode($target);
        $params = ['code' => $code];
        if ($this->execute($target, $params)) {
            $this->saveCode($target, $code);
            return true;
        } else {
            return false;
        }
    }

    /**
     * 执行
     *
     * @param $params
     * @param $target
     * @return bool
     * @throws Exception
     */
    protected function execute($target, $params)
    {
        $client = new Client($this->config['endpoint'], $this->config['access_id'], $this->config['access_key']);
        /**
         * Step 2. 获取主题引用
         */
        $topicName = "sms.topic-cn-hangzhou";
        $topic = $client->getTopicRef($topicName);
        /**
         * Step 3. 生成SMS消息属性
         */
        // 3.1 设置发送短信的签名（SMSSignName）和模板（SMSTemplateCode）
        $batchSmsAttributes = new BatchSmsAttributes($this->config['sign'], $this->config['template_code']);
        // 3.2 （如果在短信模板中定义了参数）指定短信模板中对应参数的值
        $batchSmsAttributes->addReceiver($target, $params);
        $messageAttributes = new MessageAttributes(array($batchSmsAttributes));
        /**
         * Step 4. 设置SMS消息体（必须）
         *
         * 注：目前暂时不支持消息内容为空，需要指定消息内容，不为空即可。
         */
        $messageBody = "验证码";
        /**
         * Step 5. 发布SMS消息
         */
        $request = new PublishMessageRequest($messageBody, $messageAttributes);
        try {
            $res = $topic->publishMessage($request);
            if ($res->isSucceed()) {
                return true;
            } else {
                return false;
            }

        } catch (MnsException $e) {
            return false;
        }

        /*
        $tc = new \TopClient;
        $tc->appkey = $this->config['app_key'];
        $tc->secretKey = $this->config['app_secret'];

        $req = new \AlibabaAliqinFcSmsNumSendRequest;
        $req->setSmsType("normal");
        $req->setSmsFreeSignName($this->config['sign']);
        $req->setSmsParam(json_encode($params));
        $req->setRecNum($target);
        $req->setSmsTemplateCode($templateCode);
        $resp = (array)$tc->execute($req);
        if (isset($resp['code']) && in_array($resp['sub_code'], ['isv.MOBILE_NUMBER_ILLEGAL', 'isv.MOBILE_COUNT_OVER_LIMIT'])) {
            throw new Exception(isset($resp['sub_msg']) ? $resp['sub_msg'] : '发送失败');
        } elseif (isset($resp['code']) && in_array($resp['sub_code'], ['isv.BUSINESS_LIMIT_CONTROL'])) {
            throw new Exception('发送频率过快，请稍后再试~');
        } else {
            return true;
        }*/
    }

}
