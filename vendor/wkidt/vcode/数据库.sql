
CREATE TABLE `gig_vendor_code_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号码',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '发送类型',
  `code` char(6) NOT NULL DEFAULT '' COMMENT '验证码',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否已用',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `create_ip` char(32) NOT NULL DEFAULT '' COMMENT '添加ip',
  `verify_count` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '验证次数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC COMMENT='验证码记录表'

