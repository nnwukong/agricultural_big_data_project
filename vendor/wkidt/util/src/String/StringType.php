<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/11/24 0024 16:56
// +----------------------------------------------------------------------
// | Readme: 常用类型常量定义
// +----------------------------------------------------------------------

/**
 * 手机类型
 */
defined('PHONE_TYPE') or define('PHONE_TYPE', 1);

/**
 * 邮箱类型
 */
defined('EMAIL_TYPE') or define('EMAIL_TYPE', 2);

/**
 * 身份证类型
 */
defined('ID_CARD_TYPE') or define('ID_CARD_TYPE', 3);

/**
 * 昵称类型
 */
defined('NICKNAME_TYPE') or define('NICKNAME_TYPE', 4);

/**
 * 真实姓名类型
 */
defined('REAL_NAME_TYPE') or define('REAL_NAME_TYPE', 5);

/**
 * 银行卡类型
 */
defined('BANK_CARD_TYPE') or define('BANK_CARD_TYPE', 6);

