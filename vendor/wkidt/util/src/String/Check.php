<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/11/1 0001 16:50
// +----------------------------------------------------------------------
// | Readme: 验证类
// +----------------------------------------------------------------------

namespace Wkidt\Util\String;

class Check
{

    const PHONE_NUMBER = 0;
    const PASSWORD = 1;

    protected static $types = array(
        PHONE_TYPE => '/^1\d{10}$/',
        self::PASSWORD => '/^.{6,16}$/',
    );

    /**
     * 验证
     * @param $str
     * @param $type
     * @return bool
     */
    public static function check($str, $type)
    {
        $regex = self::$types[$type];

        if (!empty($regex)) {
            $res = preg_match($regex, $str);
            return empty($res) ? false : true;
        } else {
            return false;
        }
    }

    /**
     * 检测是否只包含数字和字母
     * @param string $str 要检测的字符串
     * @param string $extra 附件字符
     * @return bool
     */
    public static function checkAZ09($str, $extra = '')
    {
        $res = preg_match('|^[0-9a-zA-Z' . $extra . ']+$|', $str);
        return $res;

    }

    /**
     * 判断是否是手机号码
     * @param $str
     * @return bool
     */
    public static function isPhone($str)
    {
        return self::check($str, PHONE_TYPE);
    }

    /**
     * @param $number
     * @return bool
     */
    public static function isIdCard($number)
    {
        //加权因子
        $wi = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
        //校验码串
        $ai = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
        //按顺序循环处理前17位
        $sigma = '';
        for ($i = 0; $i < 17; $i++) {
            //提取前17位的其中一位，并将变量类型转为实数
            $b = (int)$number{$i};

            //提取相应的加权因子
            $w = $wi[$i];

            //把从身份证号码中提取的一位数字和加权因子相乘，并累加
            $sigma += $b * $w;
        }
        //计算序号
        $snumber = $sigma % 11;

        //按照序号从校验码串中提取相应的字符。
        $check_number = $ai[$snumber];

        if ($number{17} == $check_number) {
            return true;
        } else {
            return false;
        }
    }
}

