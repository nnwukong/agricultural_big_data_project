<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/11/1 0001 16:48
// +----------------------------------------------------------------------
// | Readme: 字符串处理类
// +----------------------------------------------------------------------

namespace Wkidt\Util\String;

/**
 *
 */
class StringUtil
{


    /**
     * 隐藏字符串
     * @param $str
     * @param $type
     * @param string $symbol
     * @return string
     */
    public static function hide($str, $type = null, $symbol = '*')
    {

        $strLen = mb_strlen($str, 'utf-8');
        if ($strLen == 1) {      //一个字符或汉字的时候
            return $symbol;
        } else {
            switch ($type) {
                case PHONE_TYPE:
                    return substr_replace($str, str_repeat($symbol, 5), 2, -3);
                    break;

                case BANK_CARD_TYPE:
                    return substr_replace($str, str_repeat($symbol, 6), 2, -4);
                    break;

                case ID_CARD_TYPE:
                    return substr_replace($str, str_repeat($symbol, 6), 2, -4);
                    break;

                case REAL_NAME_TYPE:
                    return $symbol . mb_substr($str, 1, null, 'utf-8');
                    break;

                case NICKNAME_TYPE:
                    if ($strLen <= 1) {
                        return $symbol;
                    } elseif ($strLen <= 2) {
                        return $symbol . mb_substr($str, 1, null, 'utf-8');
                    } else {
                        $start = floor($strLen * 0.66);
                        return str_repeat($symbol, 3) . mb_substr($str, $start, null, 'utf-8');
                    }
                    break;
                default:
                    return $symbol;
                    break;
            }
        }
    }

}

