<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/11/28 0028 17:40
// +----------------------------------------------------------------------
// | Readme: 拼音处理类
// +----------------------------------------------------------------------

namespace Wkidt\Util\Hanzi;

use Common\Exception\RuntimeException;

class Pinyin
{

    /**
     * @功能: 最全的PHP汉字转拼音函数 （共25961字，包括 20902基本字 + 5059生僻字）
     * @版本: 1.0.0
     * @作者: wuzhaohuan <kongphp@gmail.com> <blog.520.at>
     * @时间: 2013-10-08
     * @param $str
     * @param bool|false $isfirst
     * @return string
     * @throws RuntimeException
     */
    public static function hanzi2pinyin($str, $isfirst = false)
    {
        static $pinyins;

        $str = trim($str);
        $len = strlen($str);
        if ($len < 3) return $str;

        if (!isset($pinyins)) {
            $dicFile = dirname(__FILE__) . '/pinyindata.txt';
            if (is_file($dicFile)) {
                $data = file_get_contents($dicFile);
            } else {
                throw new RuntimeException('拼音字典不存在');
            }
            $a1 = explode('|', $data);
            $pinyins = array();
            foreach ($a1 as $v) {
                $a2 = explode(':', $v);
                $pinyins[$a2[0]] = $a2[1];
            }
        }

        $rs = '';
        for ($i = 0; $i < $len; $i++) {
            $o = ord($str[$i]);
            if ($o < 0x80) {
                if (($o >= 48 && $o <= 57) || ($o >= 97 && $o <= 122)) {
                    $rs .= $str[$i]; // 0-9 a-z
                } elseif ($o >= 65 && $o <= 90) {
                    $rs .= strtolower($str[$i]); // A-Z
                } else {
                    $rs .= '_';
                }
            } else {
                $z = $str[$i] . $str[++$i] . $str[++$i];
                if (isset($pinyins[$z])) {
                    $rs .= $isfirst ? $pinyins[$z][0] : $pinyins[$z];
                } else {
                    $rs .= '_';
                }
            }
        }
        return $rs;
    }
}

