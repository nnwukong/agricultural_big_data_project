<?php

// +----------------------------------------------------------------------
// | 验证码生成类
// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team <admin@wkidt.com>
// +----------------------------------------------------------------------

namespace Wkidt\Captcha\Tester;

use think\Cache;
use think\Session;
use Wkidt\think5\exception\Exception;
use Wkidt\Captcha\Interfaces\TesterInterface;
use Wkidt\think5\request\Request;

class CheckCode implements TesterInterface
{

    /**
     * 随机因子
     *
     * @var string
     */
    private $charset = '0123456789abcdefghijklnmopqrstuvwxyz';

    /**
     * 验证码
     *
     * @var
     */
    private $_code;

    /**
     * 验证码长度
     *
     * @var int
     */
    private $codelen = 5;

    /**
     * 宽度
     *
     * @var int
     */
    private $width = 100;

    /**
     * 高度
     *
     * @var int
     */
    private $height = 40;

    /**
     * 图形资源句柄
     *
     * @var
     */
    private $img;

    /**
     * 指定的字体
     *
     * @var string
     */
    private $font;

    /**
     * 指定字体大小
     *
     * @var int
     */
    private $fontsize = 15;

    /**
     * 指定字体颜色
     *
     * @var string
     */
    private $fontcolor;

    /**
     * 设置背景色
     *
     * @var string
     */
    private $background = '#EDF7FF';

    /**
     * 构造方法初始化
     *
     * CheckCode constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->font = __DIR__ . '/../../font/elephant.ttf';

        //初始参数
        $this->width = isset($_GET['width']) ? intval($_GET['width']) : (isset($config['width']) ? $config['width'] : $this->width);
        $this->height = isset($_GET['height']) ? intval($_GET['height']) : (isset($config['height']) ? $config['width'] : $this->height);
        $this->fontsize = $this->width / $this->codelen - mt_rand(0, 3) - 1;
    }

    /**
     * 生成随机码
     *
     * @return string
     */
    private function createCode()
    {
        $code = '';
        $_len = strlen($this->charset) - 1;
        for ($i = 0; $i < $this->codelen; $i++) {
            $code .= $this->charset[mt_rand(0, $_len)];
        }
        return $code;
    }

    /**
     * 生成背景
     *
     */
    private function createBg()
    {
        $this->img = imagecreatetruecolor($this->width, $this->height);
        if (empty($this->background)) {
            $color = imagecolorallocate($this->img, mt_rand(157, 255), mt_rand(157, 255), mt_rand(157, 255));
        } else {
            //设置背景色
            $color = imagecolorallocate($this->img, hexdec(substr($this->background, 1, 2)), hexdec(substr($this->background, 3, 2)), hexdec(substr($this->background, 5, 2)));
        }
        imagefilledrectangle($this->img, 0, $this->height, $this->width, 0, $color);
    }

    /**
     * 生成文字
     *
     */
    private function createFont()
    {
        $this->_code = $this->createCode();
        $_x = $this->width / $this->codelen;
        $isFontcolor = false;
        if ($this->fontcolor && !$isFontcolor) {
            $this->fontcolor = imagecolorallocate($this->img, hexdec(substr($this->fontcolor, 1, 2)), hexdec(substr($this->fontcolor, 3, 2)), hexdec(substr($this->fontcolor, 5, 2)));
            $isFontcolor = true;
        }
        for ($i = 0; $i < $this->codelen; $i++) {
            if (!$isFontcolor) {
                $this->fontcolor = imagecolorallocate($this->img, mt_rand(0, 156), mt_rand(0, 156), mt_rand(0, 156));
            }
            imagettftext($this->img, $this->fontsize, mt_rand(-30, 30), $_x * $i + mt_rand(1, 5), $this->height / 1.4, $this->fontcolor, $this->font, $this->_code[$i]);
        }
    }

    /**
     * 生成线条、雪花
     *
     */
    private function createLine()
    {
        for ($i = 0; $i < 6; $i++) {
            $color = imagecolorallocate($this->img, mt_rand(0, 156), mt_rand(0, 156), mt_rand(0, 156));
            imageline($this->img, mt_rand(0, $this->width), mt_rand(0, $this->height), mt_rand(0, $this->width), mt_rand(0, $this->height), $color);
        }
        for ($i = 0; $i < 100; $i++) {
            $color = imagecolorallocate($this->img, mt_rand(200, 255), mt_rand(200, 255), mt_rand(200, 255));
            imagestring($this->img, mt_rand(1, 5), mt_rand(0, $this->width), mt_rand(0, $this->height), '*', $color);
        }
    }

    /**
     * 输出
     *
     * @return bool
     */
    public function outputData()
    {
        ob_clean();
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-Transfer-Encoding: binary');
        header('Content-type:image/png');

        $this->createBg();
        $this->createLine();
        $this->createFont();

        if ($sid = Request::instance()->get('sid')) {
            Cache::set(md5('captcha_' . $sid), $this->_code, 300);
        } else {
            Session::set('captcha_phrase', $this->_code);
        }

        imagepng($this->img);
        imagedestroy($this->img);
        return true;
    }

    /**
     * 验证输入，看它是否生成的代码相匹配。
     *
     * @return bool
     * @throws Exception
     */
    public function verify()
    {
        $inputPhrase = Request::instance()->param('captcha', '');
        if ($inputPhrase) {
            if ($sid = Request::instance()->param('sid')) {
                $phrase = Cache::get(md5('captcha_' . $sid));
            } else {
                $phrase = Session::get('captcha_phrase');
            }

            if ($phrase) {
                $sid ? Cache::set(md5('captcha_' . $sid), null) : Session::delete('captcha_phrase');
                if (strtolower($inputPhrase) == strtolower($phrase)) {
                    return true;
                } else {
                    throw new Exception('图形验证码错误');
                }
            } else {
                throw new Exception('图形验证码已过期');
            }
        } else {
            throw new Exception('请输入图形验证码');
        }
    }
}
