<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team 73504 <admin@wkidt.com> 2018/3/14 15:59
// +----------------------------------------------------------------------
// | Readme: 文件说明
// +----------------------------------------------------------------------

namespace Wkidt\Captcha\Tester;

use Gregwar\Captcha\CaptchaBuilder;
use think\Cache;
use think\Session;
use Wkidt\Captcha\Interfaces\TesterInterface;
use Wkidt\think5\exception\Exception;
use Wkidt\think5\request\Request;

class Gregwar implements TesterInterface
{

    /**
     * 输出图形验证码
     *
     */
    public function outputData()
    {
        header('Content-type: image/jpeg');

        $width = Request::instance()->get('width', 100);
        $height = Request::instance()->get('height', 40);

        $builder = new CaptchaBuilder();
        if ($sid = Request::instance()->get('sid')) {
            Cache::set(md5('captcha_' . $sid), $builder->getPhrase(), 300);
        } else {
            Session::set('captcha_phrase', $builder->getPhrase());
        }

        $builder->build($width, $height);
        $builder->output();
        return true;
    }

    /**
     * 验证图形验证码
     *
     * @throws Exception
     * @return bool
     */
    public function verify()
    {
        $inputPhrase = Request::instance()->param('captcha', '');
        if ($inputPhrase) {
            if ($sid = Request::instance()->param('sid')) {
                $phrase = Cache::get(md5('captcha_' . $sid));
            } else {
                $phrase = Session::get('captcha_phrase');
            }

            if ($phrase) {
                $sid ? Cache::set(md5('captcha_' . $sid), null) : Session::delete('captcha_phrase');
                $builder = new CaptchaBuilder($phrase);
                if ($builder->testPhrase($inputPhrase)) {
                    return true;
                } else {
                    throw new Exception('图形验证码错误');
                }
            } else {
                throw new Exception('图形验证码已过期');
            }
        } else {
            throw new Exception('请输入图形验证码');
        }
    }

}
