<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/6/13 18:17
// +----------------------------------------------------------------------
// | Readme: 接口
// +----------------------------------------------------------------------

namespace Wkidt\Captcha;

use think\Response;

class Api
{

    /**
     * @apiDefine secure 安全
     */

    /**
     * @api {get} /captcha 获取图形验证码
     *
     * @apiGroup secure
     *
     * @apiParam (get) {Int} [width]   宽
     * @apiParam (get) {Int} [height]  高
     * @apiParam (get) {String} [sid]  临时标识，用于不支持cookie的客户端 验证是此字段传相同的值
     *
     * @apiSuccess (SUCCESS) {Image}  field  返回生成的图片
     */
    public function getCaptcha()
    {
        $tester = Captcha::createTester();
        if (true === $data = $tester->outputData()) {
            exit();
        } elseif ($data instanceof Response) {
            return $data;
        } else {
            $response = new Response();
            $response->data($data);
            return $response;
        }
    }
}
