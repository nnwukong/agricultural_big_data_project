<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/6/18 0018 16:14
// +----------------------------------------------------------------------
// | Readme: 图形验证驱动接口
// +----------------------------------------------------------------------

namespace Wkidt\Captcha\Interfaces;

interface TesterInterface
{

    /**
     * 输出验证
     * @return mixed
     */
    public function outputData();

    /**
     * 验证
     * @return mixed
     */
    public function verify();
}
