<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/6/18 0018 16:35
// +----------------------------------------------------------------------
// | Readme: 防机器操作验证类
// +----------------------------------------------------------------------

namespace Wkidt\Captcha;

use Wkidt\think5\exception\Exception;
use think\Config;
use Wkidt\Captcha\Interfaces\TesterInterface;

class Captcha
{

    /**
     * 基本配置
     *
     * @var array
     */
    protected static $config = [
        'type' => 'Gregwar',
    ];

    /**
     * 实例缓存
     *
     * @var array
     */
    protected static $map = [];

    /**
     * 创建验证实例
     *
     * @param array $config
     * @return TesterInterface
     * @throws Exception
     */
    public static function createTester($config = [])
    {
        $config = array_merge(self::$config, Config::get('captcha') ?: [], $config);

        $key = md5(serialize($config));
        if (isset(self::$map[$key])) {
            return self::$map[$key];
        } else {
            $class = "Wkidt\\Captcha\\Tester\\" . $config['type'];
            if (class_exists($class)) {
                self::$map[$key] = new $class($config);
                return self::$map[$key];
            } else {
                throw new Exception('验证驱动不存在');
            }
        }
    }
}
