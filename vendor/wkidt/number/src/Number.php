<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/7/6 0006 20:37
// +----------------------------------------------------------------------
// | Readme: 订单号生成类
// +----------------------------------------------------------------------

namespace Wkidt\Number;

class Number
{

    /**
     * 添加自定义的参数，暂时不支持自定义参数固定长度
     * @param string $prefix 前缀
     * @param array $options 自定义的数据
     * @param bool|false $lock 是否悲观锁
     * @param int $len 流水号长度
     * @return string                       订单号
     */
    public static function getLongNo($prefix, $options = array(), $lock = false, $len = 4)
    {
        $nid = self::getNo($prefix, $lock, $len);
        if (is_array($options)) {
            $options = implode('_', $options);
        }

        return "{$nid}_{$options}";
    }

    /**
     * @param string $prefix 前缀
     * @param bool $lock 是否悲观锁，默认false 乐观锁
     * @param int $len 流水号码长度
     * @return bool|string
     */
    public static function getNo($prefix, $lock = false, $len = 4)
    {
        //获取流水号
        $date = date('Ymd', NOW_TIME);
        $number = self::getOne($prefix, $lock, $date);
        if (!$number) {
            return false;
        } else {
            //自动增加长度
            $num_len = strlen($number);
            $len = $num_len > $len ? $num_len + 1 : $len;

            //通用规则流水号
            return "{$prefix}{$date}" . substr(str_repeat('0', $len) . $number, -$len);
        }
    }

    /**
     * 获取一个新的通用订单号的流水值
     * @param string $prefix 前缀
     * @param bool $lock 是否悲观锁，默认false 乐观锁
     * @param string $date 日期字符串
     * @return string
     */
    public static function getOne($prefix, $lock = false, $date = '')
    {
        //验证前缀
        if (!preg_match('/^\w{1,30}$/', $prefix)) {
            return false;
        } else {
            $prefix = strtoupper($prefix);
        }

        //日期字符串
        $date = empty($date) ? date('Ymd', NOW_TIME) : $date;
        $number = 1;
        $orderObj = M('SysOrderNo');

        if ($lock) {
            $orderObj->startTrans();
        }

        //查询前缀
        $order = $orderObj
            ->where(array('code' => $prefix))
            ->lock($lock)
            ->find();
        if (empty($order)) {
            $res = $orderObj->add(array(
                'code' => $prefix,
                'date' => $date,
                'number' => $number,
            ));
        } else {
            if ($order['date'] == $date) {
                $number = $order['number'] + 1;

                $res = $orderObj->where(array(
                    'code' => $prefix,
                    'date' => $order['date'],
                    'number' => $order['number'],
                ))->save(array(
                    'number' => $number,
                ));
            } else {
                $res = $orderObj->where(array(
                    'code' => $prefix,
                    'date' => $order['date'],
                    'number' => $order['number'],
                ))->save(array(
                    'date' => $date,
                    'number' => $number,
                ));
            }
        }

        if ($lock) {
            $orderObj->commit();
        }

        if (!empty($res)) {
            return $number;
        } else {
            return false;
        }
    }

    /**
     * 通过一个十进制数生成一个唯一无序字符串
     * @param $int
     * @return string
     */
    public static function upset($int)
    {
        $dic = '2KATJ6IGF7OEHCS90U5N143WLMDZY8RBQZVP';
        $mask = '3096283869910302163288918216964405477353158682569057718733';
        $len = strlen($dic);
        $result = '';
        while (true) {
            if ($int < $len) {
                $result = $dic[$int] . $result;
                break;
            } else {
                $y = $int % $len;
                $result = $dic[$y] . $result;
                $int = floor($int / $len);
            }
        }
        return $result;
    }
}
