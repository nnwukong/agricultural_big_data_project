<?php

/**
 * Curl 封装
 * Contact email:735041553@qq.com
 */

namespace Wkidt\Transport;

use think\Exception;

class Curl
{
    /**
     * 参数
     * @var array
     */
    protected $options = [];

    /**
     * 默认参数
     * @var array
     */
    protected $defaultOptions = [
        CURLOPT_USERAGENT => 'PHP Curl/0.1 (http://www.wkidt.com/)',
        CURLOPT_HEADER => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 10,
    ];

    /**
     * curl 句柄
     * @var resource
     */
    protected $curl = null;

    /**
     * http 返回信息
     * @var string
     */
    protected $message = '';

    /**
     * 最后一个错误号
     * @var string
     */
    protected $curlErrorNo = 0;

    /**
     * curl 错误信息
     * @var string
     */
    protected $curlError = '';

    /**
     * 响应头
     * @var null
     */
    protected $responseHeader = null;

    /**
     * 响应体
     * @var null
     */
    protected $responseBody = null;

    /**
     * 协议版本
     * @var string
     */
    protected $protocolVersion = '';

    /**
     * 请求状态
     * @var null
     */
    protected $status = null;

    /**
     * 发送POST请求
     * @param string $url 地址
     * @param string|array $params 附带参数，可以是数组，也可以是字符串
     * @return boolean|array
     */
    public function post($url, $params)
    {
        return $this->execute('POST', $url, $params);
    }

    /**
     * GET
     * @param string $url 地址
     * @param string $params 浏览器UA
     * @return boolean
     */
    public function get($url, $params = '')
    {
        return $this->execute('GET', $url, $params);
    }

    /**
     * 执行请求
     * @param string $method 请求方式
     * @param string $url 地址
     * @param string|array $params 附带参数，可以是数组，也可以是字符串
     * @return boolean|array
     */
    public function execute($method, $url, $params = '')
    {
        $this->init();

        // 请求类型
        switch (strtolower($method)) {
            case 'post':
                $this->options[CURLOPT_POST] = true;
                $this->options[CURLOPT_POSTFIELDS] = $this->parseParams($params);
                break;
            case 'put':
                $this->options[CURLOPT_PUT] = true;
                break;
            case 'get':
                $url = $this->parseUrl($url, $params);
                break;
        }

        // 是否https
        if (false !== stripos($url, "https://")) {
            $this->options[CURLOPT_SSL_VERIFYPEER] = false;
            $this->options[CURLOPT_SSL_VERIFYHOST] = false;
        }

        // 设置url
        $this->options[CURLOPT_URL] = $url;

        // 执行并获取结果
        if (!curl_setopt_array($this->curl, $this->options)) {
            return false;
        }
        $result = $this->parseResult(curl_exec($this->curl));
        if (false === $result) {
            $this->parseError();
        }
        $this->reset();
        return $result;
    }

    /**
     * 设置ua
     * @param $userAgent
     * @return $this
     */
    public function setUserAgent($userAgent = null)
    {
        if (!is_null($userAgent)) {
            $this->options[CURLOPT_USERAGENT] = $userAgent;
        }
        return $this;
    }

    /**
     * 设置头信息
     * @param $headers
     * @param null $value
     * @return $this
     */
    public function setHeader($headers, $value = null)
    {
        if (is_array($headers) && is_null($value)) {
            $headersValue = [];
            foreach ($headers as $key => $val) {
                $headersValue[] = "{$key}: {$val}";
            }
            $this->options[CURLOPT_HTTPHEADER] = $headersValue;
        } elseif (!is_null($value)) {
            $this->options[CURLOPT_HTTPHEADER][$headers] = $value;
        }
        return $this;
    }

    /**
     * 设置cookies
     * @param $cookies
     * @return $this
     */
    public function setCookies($cookies)
    {
        if (is_array($cookies)) {
            $this->options[CURLOPT_COOKIE] = $this->parseParams($cookies, '; ', true);
        }
        return $this;
    }

    /**
     * 用户密码
     * @param $username
     * @param $password
     * @return $this
     */
    public function setAuth($username, $password)
    {
        $this->options[CURLOPT_USERPWD] = $username . ':' . $password;
        return $this;
    }

    /**
     * 获取响应头
     * @param null $name
     * @return null
     */
    public function getHeader($name = null)
    {
        if (is_null($name)) {
            return $this->responseHeader;
        } else {
            return $this->responseHeader[$name];
        }
    }

    /**
     * 获取请求体
     * @return null
     */
    public function getBody()
    {
        return $this->responseBody;
    }

    /**
     * 获取curl 错误编号
     * @return mixed
     */
    public function getCurlErrorNo()
    {
        return $this->curlErrorNo;
    }

    /**
     * 获取curl 错误信息
     * @return string
     */
    public function getCurlError()
    {
        return $this->curlError;
    }

    /**
     * 获取http 请求信息
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * 获取请求状态
     * @return null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * 初始化
     * @throws Exception
     * @return $this
     */
    public function init()
    {
        if (!function_exists('curl_init')) {
            throw new Exception('Curl extension not installed.');
        } else {
            $this->curl = curl_init();
            if (!is_resource($this->curl)) {
                throw new Exception('Curl init failure.');
            } else {
                // 合并默认选项
                foreach ($this->defaultOptions as $key => $val) {
                    isset($this->options[$key]) || $this->options[$key] = $val;
                }

                $this->responseBody = null;
                $this->responseHeader = null;
                $this->error = '';
                $this->errorNo = '';
                return $this;
            }
        }
    }

    /**
     * 重置属性
     * @return $this
     */
    public function reset()
    {
        curl_close($this->curl);
        $this->options = [];
        return $this;
    }

    /**
     * 分析结果
     * @param $result
     * @return null
     */
    protected function parseResult($result)
    {
        if (false !== $result) {
            list($headers, $this->responseBody) = preg_split('/\r\n\r\n|\n\n|\r\r/', $result, 2);

            // 解析头部信息
            $headers = preg_split('/\r\n|\n|\r/', $headers);
            list($this->protocolVersion, $this->status, $this->message) =
                explode(' ', array_shift($headers), 3);

            foreach ($headers as $value) {
                list($name, $val) = explode(':', $value, 2);
                $this->responseHeader[$name] = ltrim($val);
            }
            return $this->responseBody;
        } else {
            return false;
        }
    }

    /**
     * 分析错误
     */
    protected function parseError()
    {
        if (curl_errno($this->curl)) {
            $this->curlError = curl_error($this->curl);
            $this->curlErrorNo = curl_errno($this->curl);
        }
    }

    /**
     * 分析url
     * @param $url
     * @param string $params
     * @return string
     */
    protected function parseUrl($url, $params = '')
    {
        return $url . (false !== strpos($url, '?') ? '&' : '?') .
            $this->parseParams($params);
    }

    /**
     * 把数组连接为&k=v形式
     * @param $params
     * @param $glue
     * @param $keepEnd
     * @return string
     */
    protected function parseParams($params, $glue = '&', $keepEnd = false)
    {
        if (is_array($params)) {
            $res = '';
            foreach ($params as $k => $v) {
                $res .= "{$k}=" . urlencode($v) . $glue;
            }
            $keepEnd || $res = substr($res, 0, -strlen($glue));
            return $res;
        } else {
            return $params;
        }
    }
}
