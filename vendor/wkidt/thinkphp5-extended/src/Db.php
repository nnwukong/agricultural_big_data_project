<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/10/10 22:59
// +----------------------------------------------------------------------
// | Readme: Db 类扩展
// +----------------------------------------------------------------------

namespace Wkidt\think5;

class Db extends \think\Db
{

    /**
     * 是否在事务内
     *
     * @var bool
     */
    protected static $transactional = false;

    /**
     * 开始事务
     *
     */
    public static function startTransactional()
    {
        \think\Db::startTrans();
        self::$transactional = true;
    }

    /**
     * 提交事务
     *
     */
    public static function completeTransactional()
    {
        if (self::$transactional) {
            \think\Db::commit();
            self::$transactional = false;
        }
    }

    /**
     * 回滚事务
     *
     */
    public static function cancelTransactional()
    {
        if (self::$transactional) {
            \think\Db::rollback();
            self::$transactional = false;
        }
    }
}
