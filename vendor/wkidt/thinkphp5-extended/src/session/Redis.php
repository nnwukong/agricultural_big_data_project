<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/7/3 10:18
// +----------------------------------------------------------------------
// | Readme: redis session
// +----------------------------------------------------------------------

namespace Wkidt\think5\session;

class Redis extends \think\session\driver\Redis
{

    /**
     * 默认超时时间
     *
     * @var int
     */
    protected $defaultTimeLimit = 30;

    protected $lockTime = null;

    /**
     * 读取Session
     *
     * @access public
     * @param string $sessID
     * @return string
     */
    public function read($sessID)
    {
        // 读取时加锁
        $timeLimit = $this->getLockTime();
        $lockKey = $this->config['session_name'] . $sessID . '.lock';

        do {
            $isLock = $this->handler->setnx($lockKey, true);
            if ($isLock) {
                $this->handler->expire($lockKey, $timeLimit ?: $this->defaultTimeLimit);
            } else {
                usleep(50000);
            }
        } while (!$isLock);
        return (string)$this->handler->get($this->config['session_name'] . $sessID);
    }

    /**
     * 获取加锁时间
     *
     * @return int
     */
    protected function getLockTime()
    {
        if (is_null($this->lockTime)) {
            $ini = ini_get_all();
            $this->lockTime = isset($ini['max_execution_time']['global_value']) ? $ini['max_execution_time']['global_value'] : $this->defaultTimeLimit;
        }
        return $this->lockTime;
    }

    /**
     * 写入Session
     *
     * @access public
     * @param string $sessID
     * @param String $sessData
     * @return bool
     */
    public function write($sessID, $sessData)
    {
        if ($this->config['expire'] > 0) {
            $result = $this->handler->setex($this->config['session_name'] . $sessID, $this->config['expire'], $sessData);
        } else {
            $result = $this->handler->set($this->config['session_name'] . $sessID, $sessData);
        }

        // 解锁session
        $this->handler->delete($this->config['session_name'] . $sessID . '.lock');
        return $result;
    }

}
