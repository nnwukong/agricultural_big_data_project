<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/5/12 11:58
// +----------------------------------------------------------------------
// | Readme: 树形模型
// +----------------------------------------------------------------------

namespace Wkidt\think5\model;

use Wkidt\think5\exception\Exception;
use Wkidt\think5\exception\TreeCycleException;

class TreeModel extends Model
{

    /**
     * 根id
     *
     * @var int
     */
    protected $rootId = 0;

    /**
     * 树关联字段
     *
     * @var string
     */
    protected $relationField = 'pid';

    /**
     * 初始化
     */
    public static function init()
    {
        parent::init();

        static::event('before_delete', function (TreeModel $model) {
            if ($children = $model->getChildren()) {
                static::destroy($children);
            }
        });

        static::event('before_update', function (TreeModel $model) {
            if (in_array($model->getData($model->relationField), $model->getDescendants())) {
                throw new TreeCycleException();
            }
        });
    }

    /**
     * 获取自己和所有后代
     *
     * @param int $ids
     * @return array|int
     * @throws TreeCycleException
     */
    public function getSelfAndDescendants($ids = 0)
    {
        if (empty($ids)) {
            $ids = [$this->getData($this->getPk())];
        } elseif (is_string($ids)) {
            $ids = explode(',', $ids);
        }
        return array_merge($ids, $this->getDescendants($ids));
    }


    /**
     * 获取所有后代
     *
     * @param int $ids
     * @param bool $clear
     * @return array|int
     * @throws TreeCycleException
     */
    public function getDescendants($ids = 0, $clear = true)
    {
        static $descendants = [];
        if (empty($ids)) {
            $ids = [$this->getAttr($this->getPk())];
        } elseif (is_string($ids)) {
            $ids = explode(',', $ids);
        }
        $clear && $descendants = [];

        $children = [];
        foreach ($ids as $id) {
            if (in_array($id, $descendants)) {
                throw new TreeCycleException();
            }
            $children = array_merge($children, $this->getChildren($id));
        }

        !$clear && $descendants = array_merge($descendants, $ids);
        if (empty($children)) {
            return $descendants;
        } else {
            return $this->getDescendants($children, false);
        }
    }

    /**
     * 获取所有子级
     *
     * @param $id
     * @return array
     */
    public function getChildren($id = 0)
    {
        $id = $id ?: $this->getData($this->getPk());
        return $this->where($this->relationField, $id)
            ->column($this->getPk());
    }

    /**
     * 获取所有祖先
     *
     * @param int $ids
     * @param bool $clear
     * @return array|mixed
     * @throws Exception
     * @throws TreeCycleException
     */
    public function getAncestors($ids = 0, $clear = true)
    {
        // 如果是数组
        if (is_array($ids) || (is_string($ids) && false !== strpos($ids, ','))) {
            $all = [];
            if (is_string($ids) && false !== strpos($ids, ',')) {
                $ids = explode(',', $ids);
            }
            foreach ($ids as $id) {
                $all = array_merge($all, $this->getAncestors($id));
            }
            return array_unique($all);
        }

        static $ancestors = [];
        $clear && $ancestors = [];
        $model = $ids ? static::get($ids) : $this;
        if (is_null($model)) {
            return $ancestors;
            // throw new Exception('树结构错误', 'tree_structure_error');
        }

        $pid = $model->getParent();
        if (in_array($pid, $ancestors)) {
            throw new TreeCycleException();
        }
        $ancestors[] = $pid;
        return $pid == $this->rootId ? $ancestors : $model->getAncestors($pid, false);
    }

    /**
     * 获取父级id
     *
     * @return int
     */
    public function getParent()
    {
        if ($pid = $this->getData($this->relationField)) {
            return $pid;
        } else {
            return $this->rootId;
        }
    }

    /**
     * 获取树关联字段
     *
     * @return string
     */
    public function getRelationField()
    {
        return $this->relationField;
    }

    /**
     * 获取树关联字段
     *
     * @param $field
     * @return $this
     */
    public function setRelationField($field)
    {
        $this->relationField = $field;
        return $this;
    }
}
