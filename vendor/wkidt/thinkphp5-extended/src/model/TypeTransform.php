<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/6/12 9:30
// +----------------------------------------------------------------------
// | Readme: 类型转换特征
// +----------------------------------------------------------------------

namespace Wkidt\think5\model;

use think\Config;

trait TypeTransform
{


    /**
     * 图片类型写入转换
     *
     * @param $value
     * @return string
     */
    protected function imageWriteTransform($value)
    {
        $value = parse_url($value);
        return isset($value['path']) ? $value['path'] : '';
    }

    /**
     * 图片类型读取转换
     *
     * @param $value
     * @return string
     */
    protected function imageReadTransform($value)
    {
        if (false == $value) {
            return $value;
        } else {
            $domain = Config::get('file.domain');
            return "{$domain}{$value}";
        }
    }

    /**
     * 多图片类型写入转换
     *
     * @param $value
     * @return string
     */
    protected function imagesWriteTransform($value)
    {
        $value = json_decode($value, true);
        if (is_array($value)) {
            $result = [];
            foreach ($value as &$val) {
                $urlInfo = parse_url(isset($val['url']) ? $val['url'] : '');
                $result[] = [
                    'url' => isset($urlInfo['path']) ? $urlInfo['path'] : '',
                    'remark' => isset($val['remark']) ? $val['remark'] : '',
                ];
            }
            $value = json_encode($result);
        }
        return $value;
    }

    /**
     * 多图片类型读取转换
     *
     * @param $value
     * @return string
     */
    protected function imagesReadTransform($value)
    {
        $domain = Config::get('file.domain');
        $value = json_decode($value, true);
        if (is_array($value)) {
            foreach ($value as &$val) {
                $val['url'] = $domain . $val['url'];
            }
        }
        return $value;
    }

    /**
     * 字典类型转换
     *
     * @param $value
     * @param $data
     * @param $options
     * @return mixed
     */
    protected function dictionaryReadTransform($value, $options, &$data)
    {
        if (isset($options['field']) || isset($options['key'])) {
            $data[$options['field']] = dictionary($options['key'], $value, '');
        }
        return $value;
    }

}
