<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/4/25 18:39
// +----------------------------------------------------------------------
// | Readme: 自定义异常
// +----------------------------------------------------------------------

namespace Wkidt\think5\exception;

use Throwable;

class Exception extends \Exception
{

    /**
     * 错误码
     *
     * @var string
     */
    protected $code = 'error';

    /**
     * 错误信息
     *
     * @var string
     */
    protected $message = 'operation failed';

    /**
     * 重写实例化方法
     *
     * Exception constructor.
     * @param string $message
     * @param string $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = '', Throwable $previous = null)
    {
        $this->code = $code ?: $this->code;
        $this->message = $message ?: $this->message;

        parent::__construct($this->message, (int)$this->code, $previous);
    }
}
