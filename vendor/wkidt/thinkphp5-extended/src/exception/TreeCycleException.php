<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/5/12 17:37
// +----------------------------------------------------------------------
// | Readme: 树结构循环异常
// +----------------------------------------------------------------------

namespace Wkidt\think5\exception;

class TreeCycleException extends Exception
{
    protected $code = 'tree_cycle_exception';

    protected $message = 'tree cycle exception';
}
