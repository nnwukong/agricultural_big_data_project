<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/4/25 23:11
// +----------------------------------------------------------------------
// | Readme: 异常处理类
// +----------------------------------------------------------------------

namespace Wkidt\think5\exception;

use app\system\logic\Event;
use think\App;
use think\Config;
use think\exception\ValidateException;
use think\View as ViewTemplate;
use Wkidt\think5\request\Request;
use Wkidt\think5\response\Response;

class Handle extends \think\exception\Handle
{

    /**
     * 已希望的数据类型响应
     *
     * @param \Exception $exception
     * @return \think\Response|Response
     */
    protected function convertExceptionToResponse(\Exception $exception)
    {
        if ($exception instanceof Exception || $exception instanceof ValidateException || !App::$debug) {
            $data = [
                'code' => $this->getCode($exception) ?: 'error',
                'info' => $this->getMessage($exception),
            ];

            if (!Config::get('show_error_msg') && !App::$debug) {
                $data['info'] = Config::get('error_message');
            }
            if ('html' == Request::instance()->expectResponseType()) {
                $response = Response::create(ViewTemplate::instance(Config::get('template'))->fetch(Config::get('dispatch_error_tmpl'), $data));
            } else {
                $response = Response::create($data);
            }
        } else {
            $response = parent::convertExceptionToResponse($exception);
        }

        Event::trigger('exception_to_response', $exception, $response);
        return $response;
    }

    /**
     * 汇报异常
     *
     * @inheritdoc
     */
    public function report(\Exception $exception)
    {
        Event::trigger('report_exception', $exception);

        parent::report($exception);
    }
}
