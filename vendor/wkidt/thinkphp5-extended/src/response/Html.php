<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/5/14 11:05
// +----------------------------------------------------------------------
// | Readme: Html 响应类型
// +----------------------------------------------------------------------

namespace Wkidt\think5\response;

use think\Config;
use think\View;

class Html extends Response
{

    /**
     * 如果html 传入的$data是数组则经过模板解析再返回
     *
     * @param mixed $data
     * @return mixed
     */
    public function output($data)
    {

        if ($data instanceof \JsonSerializable || is_array($data)) {
            $this->data(View::instance(Config::get('template'), $this->options)->fetch('', $data));
        } else {
            $this->data((string)$data);
        }
        return parent::output($this->data);
    }
}
