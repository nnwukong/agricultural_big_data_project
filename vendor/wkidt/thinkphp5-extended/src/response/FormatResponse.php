<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/4/26 22:08
// +----------------------------------------------------------------------
// | Readme: 响应数据规范
// +----------------------------------------------------------------------

namespace Wkidt\think5\response;

trait FormatResponse
{

    /**
     * 获取输出数据
     * @return mixed
     */
    public function getContent()
    {
        if (null == $this->content) {
            $content = $this->toTemplate()->output($this->data);

            if (null !== $content && !is_string($content) && !is_numeric($content) && !is_callable([
                    $content,
                    '__toString',
                ])
            ) {
                throw new \InvalidArgumentException(sprintf('variable type error： %s', gettype($content)));
            }

            $this->content = (string)$content;
        }
        return $this->content;
    }

    /**
     * 数据模板化
     *
     * @return $this
     */
    public function toTemplate()
    {
        if (is_resource($this->data)) {
            $this->data = true;
        }
        if (empty($this->data) && false !== $this->data) {
            $this->data = ['code' => 'no_data', 'info' => '暂无数据'];
        }

        switch (gettype($this->data)) {
            case 'boolean':
                $this->data = $this->data ? ['code' => 'success', 'info' => '操作成功'] : ['code' => 'error', 'info' => '操作失败'];
                break;
            case 'array':
                $this->data = array_merge(['code' => 'success', 'info' => ((!isset($this->data['code']) || 'success' == $this->data['code']) ? '操作成功' : '操作失败')], $this->data);
                break;
            case 'string':
                $this->data = [
                    'code' => 'success',
                    'info' => '操作成功',
                    'data' => $this->data
                ];
                break;
            default:
                $this->data = [
                    'code' => 'success',
                    'info' => '操作成功',
                    'data' => $this->data
                ];
                break;
        }

        if (isset($this->data['code'])) {
            $this->data['code'] = strtoupper($this->data['code']);
        }
        return $this;
    }

}