<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2017 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

namespace Wkidt\think5\response;

use Wkidt\think5\request\Request;

class Response extends \think\Response
{
    /**
     * 创建Response对象
     *
     * @access public
     * @param mixed $data 输出数据
     * @param string $type 输出类型
     * @param int $code
     * @param array $header
     * @param array $options 输出参数
     * @return Response
     */
    public static function create($data = '', $type = '', $code = 200, array $header = [], $options = [])
    {
        $type = self::isAllowResponseType($type) ? strtolower($type) : Request::instance()->expectResponseType();

        $class = "Wkidt\\think5\\response\\" . ucfirst($type);
        if (class_exists($class)) {
            $response = new $class($data, $code, $header, $options);
        } else {
            $response = new static($data, $code, $header, $options);
        }

        return $response;
    }


    /**
     * 判断是否是允许的响应类型
     *
     * @param $type
     * @return bool
     */
    public static function isAllowResponseType($type)
    {
        if (class_exists("Wkidt\\think5\\response\\" . ucfirst($type))) {
            return true;
        } else {
            return false;
        }
    }

}
