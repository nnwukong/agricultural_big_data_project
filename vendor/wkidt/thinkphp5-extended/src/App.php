<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/9/22 11:32
// +----------------------------------------------------------------------
// | Readme: 对 think\App 的修改
// +----------------------------------------------------------------------

namespace Wkidt\think5;

use Wkidt\think5\request\Request;
use Wkidt\think5\exception\Exception;
use Wkidt\think5\response\Response;
use think\Route;
use think\Lang;
use think\Log;
use think\Hook;
use think\Loader;
use think\exception\HttpResponseException;

class App extends \think\App
{

    /**
     * 执行应用程序
     * @access public
     * @param \think\Request $request Request对象
     * @return Response
     * @throws Exception
     */
    public static function run(\think\Request $request = null)
    {
        is_null($request) && $request = Request::instance();

        try {
            $config = self::initCommon();
            if (defined('BIND_MODULE')) {
                // 模块/控制器绑定
                BIND_MODULE && Route::bind(BIND_MODULE);
            } elseif ($config['auto_bind_module']) {
                // 入口自动绑定
                $name = pathinfo($request->baseFile(), PATHINFO_FILENAME);
                if ($name && 'index' != $name && is_dir(APP_PATH . $name)) {
                    Route::bind($name);
                }
            }

            $request->filter($config['default_filter']);

            // 默认语言
            Lang::range($config['default_lang']);
            if ($config['lang_switch_on']) {
                // 开启多语言机制 检测当前语言
                Lang::detect();
            }
            $request->langset(Lang::range());

            // 加载系统语言包
            Lang::load([
                THINK_PATH . 'lang' . DS . $request->langset() . EXT,
                APP_PATH . 'lang' . DS . $request->langset() . EXT,
            ]);

            // 获取应用调度信息
            $dispatch = self::$dispatch;
            if (empty($dispatch)) {
                // 进行URL路由检测
                $dispatch = self::routeCheck($request, $config);
            }
            // 记录当前调度信息
            $request->dispatch($dispatch);

            // 记录路由和请求信息
            if (self::$debug) {
                Log::record('[ ROUTE ] ' . var_export($dispatch, true), 'info');
                Log::record('[ HEADER ] ' . var_export($request->header(), true), 'info');
                Log::record('[ PARAM ] ' . var_export($request->param(), true), 'info');
            }

            // 监听app_begin
            Hook::listen('app_begin', $dispatch);
            // 请求缓存检查
            $request->cache($config['request_cache'], $config['request_cache_expire'], $config['request_cache_except']);

            $data = self::exec($dispatch, $config);
        } catch (HttpResponseException $exception) {
            $data = $exception->getResponse();
        }

        // 清空类的实例化
        Loader::clearInstance();
        // 输出数据到客户端
        if ($data instanceof \think\Response) {
            $response = $data;
        } else {
            $response = Response::create($data);
        }

        // 监听app_end
        Hook::listen('app_end', $response);

        return $response;
    }
}
