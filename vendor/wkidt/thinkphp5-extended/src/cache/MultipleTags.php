<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/9/24 1:28
// +----------------------------------------------------------------------
// | Readme: 多标签特性
// +----------------------------------------------------------------------

namespace Wkidt\think5\cache;

trait MultipleTags
{
    /**
     * 更新标签
     *
     * @access public
     * @param string $name 缓存标识
     * @return void
     */
    protected function setTagItem($name)
    {
        if ($this->tag) {
            if (!is_array($this->tag)) {
                if (false === strpos($this->tag, ',')) {
                    $this->tag = [$this->tag];
                } else {
                    $this->tag = explode(',', $this->tag);
                }
            }
            foreach ($this->tag as $tag) {
                $key = 'tag_' . md5($tag);
                if ($this->has($key)) {
                    $value = $this->get($key);
                    $value .= ',' . $name;
                } else {
                    $value = $name;
                }
                $this->set($key, $value);
            }
            $this->tag = null;
        }
    }
}
