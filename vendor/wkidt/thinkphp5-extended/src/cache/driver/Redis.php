<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/10/10 18:19
// +----------------------------------------------------------------------
// | Readme: redis 缓存
// +----------------------------------------------------------------------

namespace Wkidt\think5\cache\driver;

use Wkidt\think5\cache\MultipleTags;

class Redis extends \think\cache\driver\Redis
{
    use MultipleTags;

}
