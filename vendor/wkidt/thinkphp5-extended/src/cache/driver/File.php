<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/9/24 1:29
// +----------------------------------------------------------------------
// | Readme: 文件缓存
// +----------------------------------------------------------------------

namespace Wkidt\think5\cache\driver;

use Wkidt\think5\cache\MultipleTags;

class File extends \think\cache\driver\File
{
    use MultipleTags;


}
