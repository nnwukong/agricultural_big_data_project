<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2017 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

namespace Wkidt\think5\request;

use think\Config;
use think\Hook;
use Wkidt\think5\response\Response;

class Request extends \think\Request
{

    const CLIENT_IOS = 'Ios';

    const CLIENT_ANDROID = 'Android';

    const CLIENT_WEIXIN = 'Weixin';

    const CLIENT_MOBILE = 'Mobile';

    const CLIENT_PC = 'pc';

    /**
     * 期望的响应类型
     *
     * @var null
     */
    protected $responseType = null;

    /**
     * 客户端类型
     *
     * @var null
     */
    protected $clientType = null;

    /**
     * 初始化
     *
     * @access public
     * @param array $options 参数
     * @return Request
     */
    public static function instance($options = [])
    {
        if (is_null(self::$instance)) {
            self::$instance = new static($options);
        }
        return self::$instance;
    }

    /**
     * 期望的响应类型
     *
     * @return mixed|string
     */
    public function expectResponseType()
    {
        if (is_null($this->responseType)) {
            $type = Hook::listen('parse_response_type', $type, null, true);
            if (Response::isAllowResponseType($type)) {
                $this->responseType = strtolower($type);
                return $this->responseType;
            }

            // url后缀
            $ext = strtolower($this->ext());
            if (Response::isAllowResponseType($ext)) {
                $this->responseType = strtolower($ext);
                return $this->responseType;
            }

            // 路由配置
            $routeInfo = $this->routeInfo();
            $type = isset($routeInfo['option']['return_type']) ? $routeInfo['option']['return_type'] : '';
            if (Response::isAllowResponseType($type)) {
                $this->responseType = strtolower($type);
                return $this->responseType;
            }

            // header传值
            $headerType = strtolower($this->header('RESPONSE_TYPE'));
            if (Response::isAllowResponseType($headerType)) {
                $this->responseType = strtolower($headerType);
                return $this->responseType;
            }

            // 请求参数中传响应类型
            $method = $this->method();
            $paramType = strtolower($this->$method('response_type'));
            if (Response::isAllowResponseType($paramType)) {
                $this->responseType = strtolower($paramType);
                return $this->responseType;
            } else {
                $this->responseType = strtolower(Config::get('default_return_type'));
                return $this->responseType;
            }
        } else {
            return $this->responseType;
        }
    }

    /**
     * 返回客户端类型
     *
     * @return string
     */
    public function getClientType()
    {
        if (isset($_GET['user_agent'])) {
            $_SERVER['HTTP_USER_AGENT'] = $_GET['user_agent'];
        } elseif (!isset($_SERVER['HTTP_USER_AGENT'])) {
            $_SERVER['HTTP_USER_AGENT'] = 'undefined';
        }

        if (is_null($this->clientType)) {
            if (preg_match('/ios app/i', $_SERVER['HTTP_USER_AGENT'])) {
                $this->clientType = self::CLIENT_IOS;
            } elseif (preg_match('/android app/i', $_SERVER['HTTP_USER_AGENT'])) {
                $this->clientType = self::CLIENT_ANDROID;
            } elseif (strpos($_SERVER['HTTP_USER_AGENT'], 'icroMessenger')) {
                $this->clientType = self::CLIENT_WEIXIN;
            } elseif (Request::instance()->isMobile()) {
                $this->clientType = self::CLIENT_MOBILE;
            } else {
                $this->clientType = self::CLIENT_PC;
            }
        }
        return $this->clientType;
    }

    /**
     * 获取access token
     *
     * @return bool
     */
    public function getAccessToken()
    {
        $accessToken = isset($_SERVER['HTTP_ACCESS_TOKEN']) ? trim($_SERVER['HTTP_ACCESS_TOKEN']) : false;
        if (empty($accessToken)) {
            $accessToken = $this->param('access_token', '', 'trim');
        }
        if (empty($accessToken)) {
            $accessToken = $this->get('access_token', '', 'trim');
        }
        return $accessToken ?: false;
    }
}
