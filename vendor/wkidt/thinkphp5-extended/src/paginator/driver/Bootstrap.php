<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/5/27 17:41
// +----------------------------------------------------------------------
// | Readme: 分页类
// +----------------------------------------------------------------------

namespace Wkidt\think5\paginator\driver;

class Bootstrap extends \think\paginator\driver\Bootstrap
{

    /**
     * 获取分页信息
     *
     * @return array
     */
    public function getPageInfo()
    {
        return [
            'page_count' => $this->lastPage,
            'page_index' => $this->currentPage(),
            'record_count' => $this->total()
        ];
    }
}
