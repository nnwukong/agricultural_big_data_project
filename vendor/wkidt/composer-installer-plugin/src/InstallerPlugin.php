<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/9/29 0029 15:53
// +----------------------------------------------------------------------
// | Readme: 文件说明
// +----------------------------------------------------------------------

namespace Wkidt\Composer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Plugin\Capable;
use Wkidt\Composer\Installer\LibraryInstaller;
use Wkidt\Composer\Installer\ModuleInstaller;

class InstallerPlugin implements PluginInterface, Capable
{

    public function activate(Composer $composer, IOInterface $io)
    {
        $installationManager = $composer->getInstallationManager();

        // 注册模块安装程序
        $moduleInstaller = new ModuleInstaller($io, $composer);
        $installationManager->addInstaller($moduleInstaller);

        // 注册扩展安装程序

        // 注册库安装程序
        $libraryInstaller = new LibraryInstaller($io, $composer);
        $installationManager->addInstaller($libraryInstaller);

    }

    /**
     * 命令提供者
     * @return array
     */
    public function getCapabilities()
    {
        return array(
            'Composer\Plugin\Capability\CommandProvider' => 'Wkidt\Composer\CommandProvider',
        );
    }
}