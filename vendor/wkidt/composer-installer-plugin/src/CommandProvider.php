<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/6 0006 22:44
// +----------------------------------------------------------------------
// | Readme: 命令提供类
// +----------------------------------------------------------------------

namespace Wkidt\Composer;

use Composer\Plugin\Capability\CommandProvider as CommandProviderCapability;
use Wkidt\Composer\Command\InitDBCommand;

class CommandProvider implements CommandProviderCapability
{
    public function __construct(array $args)
    {

    }

    public function getCommands()
    {
        return array(new InitDBCommand);
    }
}
