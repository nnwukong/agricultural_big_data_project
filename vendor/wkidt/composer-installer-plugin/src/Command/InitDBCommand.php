<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/7 0007 10:59
// +----------------------------------------------------------------------
// | Readme: 初始化数据库命令
// +----------------------------------------------------------------------

namespace Wkidt\Composer\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Composer\Command\BaseCommand;

class InitDBCommand extends BaseCommand{

    protected function configure()
    {
        $this->setName('init-db');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Do not implement!');

        return 5;
    }
}