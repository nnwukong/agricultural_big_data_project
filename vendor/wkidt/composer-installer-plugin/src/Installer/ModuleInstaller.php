<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/9/29 0029 16:26
// +----------------------------------------------------------------------
// | Readme: 文件说明
// +----------------------------------------------------------------------

namespace Wkidt\Composer\Installer;

use Composer\Package\PackageInterface;
use Composer\Repository\InstalledRepositoryInterface;

class ModuleInstaller extends Installer
{
    /**
     * 是否是模块类型
     * @param string $packageType
     * @return bool
     */
    public function supports($packageType)
    {
        $packageType = strtolower($packageType);
        return $packageType == 'wkidt-module';
    }

    /**
     * 获取安装路径
     * @param PackageInterface $package
     * @return string
     */
    public function getInstallPath(PackageInterface $package)
    {
        $extra = $package->getExtra();
        if (isset($extra['module-name']) && !empty($extra['module-name'])) {
            $name = ucwords($extra['module-name']);
        } else {
            $name = explode('/', $package->getPrettyName());
            if (false === strpos($name[1], '-')) {
                $name = ucwords($name[1]);
            }else{
                $name = explode('-', $name[1]);
                $name = ucwords($name[0]);
            }
        }

        return "Application/{$name}";
    }

    /**
     * 安装处理
     * @param InstalledRepositoryInterface $repo
     * @param PackageInterface $package
     */
    public function install(InstalledRepositoryInterface $repo, PackageInterface $package){
        parent::install($repo, $package);

        $this->cleanInstallFiles($package);
    }

}
