<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/11/18 0018 11:31
// +----------------------------------------------------------------------
// | Readme: 安装类
// +----------------------------------------------------------------------

namespace Wkidt\Composer\Installer;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;
use Composer\Script\PackageEvent;
use Composer\Util\Filesystem;

class Installer extends LibraryInstaller{


    /**
     * 清理安装文件
     * @param PackageInterface $package
     */
    protected function cleanInstallFiles(PackageInterface $package){
        // 删除.git 目录
        $path = $this->getInstallPath($package);
        $this->filesystem->removeDirectory($path.'/.git');

        // 删除安装插件目录下的.git 目录
        $path = $this->vendorDir.'/wkidt/composer-installer-plugin/.git';
        $this->filesystem->removeDirectory($path);
    }


    /**
     * 安装时执行安装目录清理
     * @param PackageEvent $event
     */
    public static function packageInstallEvent(PackageEvent $event)
    {
        $composer = $event->getComposer();
        $path = $composer->getConfig()->get('vendor-dir');
        $gitPath = $path.'/wkidt/composer-installer-plugin/.git';

        if (is_dir($gitPath)) {
            $filesystem = new Filesystem();
            $filesystem->removeDirectory($gitPath);
        }
    }
}
