<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/5 0005 15:11
// +----------------------------------------------------------------------
// | Readme: 文件说明
// +----------------------------------------------------------------------

namespace Wkidt\Composer\Installer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller as ComposerLibraryInstaller;
use Composer\Repository\InstalledRepositoryInterface;

class LibraryInstaller extends Installer
{

    /**
     * 类型匹配
     * @param string $packageType
     * @return bool
     */
    public function supports($packageType)
    {
        $packageType = strtolower($packageType);
        return $packageType == 'wkidt-library';
    }

    /**
     * 安装处理
     * @param InstalledRepositoryInterface $repo
     * @param PackageInterface $package
     */
    public function install(InstalledRepositoryInterface $repo, PackageInterface $package){
        parent::install($repo, $package);

        $this->cleanInstallFiles($package);
    }

    /**
     * @param InstalledRepositoryInterface $repo
     * @param PackageInterface $initial
     * @param PackageInterface $target
     */
    public function update(InstalledRepositoryInterface $repo, PackageInterface $initial, PackageInterface $target)
    {
        parent::update($repo, $initial, $target);

        $this->cleanInstallFiles($target);
    }
}
