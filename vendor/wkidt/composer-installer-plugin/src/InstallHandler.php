<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/8 0008 10:09
// +----------------------------------------------------------------------
// | Readme: 安装配置处理程序
// +----------------------------------------------------------------------

namespace Wkidt\Composer;

use Composer\Installer\InstallerInterface;
use Composer\Package\PackageInterface;

class InstallHandler {

    protected $installer = null;

    /**
     * 获取安装器
     * @param InstallerInterface $installer
     * @param PackageInterface $package
     */
    /*
    public function __construct(InstallerInterface $installer, PackageInterface $package){
        $this->installer = $installer;
        $this->package = $package;
    }*/

    /**
     * 处理安装配置
     * @param string $file
     */
    public function install($file = ''){

        if( file_exists($file) ){
            $config = include $file;
            if( isset($config['config']) ){
                $this->joinConfig($config['config']);
            }


        }
    }

    /**
     * 卸载
     * @param string $file
     */
    public function uninstall($file = ''){
        if( file_exists($file) ){
            $config = include $file;
            if( isset($config['config']) ){
                $this->unjoinConfig($config['config']);
            }


        }
    }

    /**
     * @param $config
     */
    protected function joinConfig($config){
        $root = 'e:\composer-test';
        $configFile = $root.'\Common\Conf\extension.php';
        if( is_file($configFile) ){
            try {
                $original = include $configFile;
                if (!empty($original) && is_array($original)) {
                    $config = array_merge($original, $config);
                }
            }catch (\Exception $e){

            }
        }

        $contents = '<?php'.PHP_EOL.PHP_EOL.'return '.var_export($config, true).';';
        $res = file_put_contents($configFile, $contents);
    }

    /**
     * @param $config
     */
    protected function unjoinConfig($config){
        $root = 'e:\composer-test';
        $configFile = $root.'\Common\Conf\extension.php';
        if( is_file($configFile) ){
            try {
                $original = include $configFile;
                if (!empty($original) && is_array($original) ) {
                    $newConfig = array_diff_key($original, $config);
                }
            }catch (\Exception $e){

            }
        }

        $contents = '<?php'.PHP_EOL.PHP_EOL.'return '.var_export($newConfig, true).';';
        $res = file_put_contents($configFile, $contents);
    }


    /**
     * 处理文件移动
     * @param $files
     * @return void
     */
    private function parseMove($files){
        // 检查文件是否存在
        if( !is_array($files) ){
            return ;
        }
        $move_list = [];
        $cover_list = [];
        foreach( $files as $key=>$val ){
            if( !file_exists($key) ){
                continue;
            }

            $target = explode('|', $val);
            foreach($target as $v){
                if( !file_exists($v) ){
                    $move_list[$key] = $v;
                    continue;
                }
            }
            $move_list[$key] = $target[0];
            $cover_list[$key] = $target[0];
        }

        // 询问是否覆盖
        $io = $this->installer->getIO();

    }
}