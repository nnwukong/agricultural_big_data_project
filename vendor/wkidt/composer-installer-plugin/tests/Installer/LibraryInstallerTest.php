<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/11/14 0014 10:40
// +----------------------------------------------------------------------
// | Readme: 库安装测试
// +----------------------------------------------------------------------

require_once '../../../../autoload.php';

use PHPUnit\Framework\TestCase;
use Composer\Package\CompletePackage;
use Composer\Repository\InstalledFilesystemRepository;
use Wkidt\Composer\Installer\LibraryInstaller;
use Composer\Json\JsonFile;
use Composer\IO\NullIO;
use Composer\Composer;
use Composer\Factory;
use Composer\Repository\ComposerRepository;
use Composer\Package\Loader\ArrayLoader;


class LibraryInstallerTest extends TestCase {


    public function testInstall(){
        $factory = new Factory();
        $repositories = [
            'repositories'=>
                [
                    [
                        'type'=>'vcs',
                        'url'=>'https://git.oschina.net/wkphp/curl-library.git'
                    ]
                ]
        ];

        $jsonFile = new JsonFile('e:\composer-test\vendor\composer\install.json');
        $repo = new InstalledFilesystemRepository($jsonFile);

        //$config = $factory->createConfig(null, 'e:\composer-test');
        $composer = $factory->createComposer(new NullIO(), null, false, 'e:\composer-test\\');

        $packages = $repo->getPackages();

        /*
        $composer = $factory->createComposer(new NullIO(), $repositories);
        $repo = $composer->getRepositoryManager()->getLocalRepository();
        $package = $repo->getPackages();

        $library = new LibraryInstaller(new NullIO(), $composer);
        $library->install($repo, $package);
        */
    }
}

