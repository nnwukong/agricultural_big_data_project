<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/6/10 12:01
// +----------------------------------------------------------------------
// | Readme: 访问接口
// +----------------------------------------------------------------------

namespace Wkidt\FileManager;

use think\Request;
use think\Response;

class Api
{

    /**
     * 上传附件
     *
     */
    public function upload()
    {
        $client = FileManager::createClient();

        // 支持base64 上传
        $post = Request::instance()->post();
        $tempDir = sys_get_temp_dir();
        foreach ($post as $k => $item) {
            $info = [];
            if (preg_match('/^data:image\/(\w{1,5});base64,(.*)/', (string)$item, $info)) {
                if (isset($info['1']) && isset($info[2])) {
                    try {
                        $key = md5($info[2]);
                        file_put_contents("$tempDir/{$key}.{$info['1']}", base64_decode($info[2]));
                        if ($size = filesize("$tempDir/{$key}.{$info['1']}")) {
                            $_FILES[$k] = [
                                'name' => "{$key}.{$info['1']}",
                                'tmp_name' => "$tempDir/{$key}.{$info['1']}",
                            ];
                        }
                    } catch (\Exception $e) {
                        continue;
                    }
                }
            }
        }

        $result = [];
        foreach ($_FILES as $file) {
            $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
            $filename = md5_file($file['tmp_name']) . '.' . strtolower($ext);

            $result[] = $client->upload($file['tmp_name'], $filename);
        }
        if (false == $result) {
            return Response::create(json_encode(['code' => 'no_file', 'info' => '没有选择任何文件']), 'html');
        } else {
            return Response::create(json_encode(['code'=>'success', 'info'=>'上传成功', 'data' => $result]), 'html');
        }
    }

    /**
     * 获取上传凭证
     *
     */
    public function getUploadAuth()
    {

        $client = FileManager::createClient();
        return $client->getUploadAuth();
    }
}
