<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/6/10 11:56
// +----------------------------------------------------------------------
// | Readme: 文件管理客户端接口
// +----------------------------------------------------------------------

namespace Wkidt\FileManager\Interfaces;

interface FileManagerClient
{

    /**
     * 单文件上传
     *
     * @param $filePath
     * @param null $filename
     * @return mixed
     */
    public function upload($filePath, $filename = null);

    /**
     * 获取临时上传凭证
     *
     * @return mixed
     */
    public function getUploadAuth();
}
