<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/4/1 11:53
// +----------------------------------------------------------------------
// | Readme: 文件上传类
// +----------------------------------------------------------------------

namespace Wkidt\Upload;

use Think\Hook;
use Wkidt\Base\Exception\RuntimeException;

class Upload
{

    /**
     * 上传文件
     * @param array $data
     * @return array
     * @throws object RuntimeException
     */
    public static function upload($data = array())
    {
        $setting = C('UPLOAD_SETTING');
        //检查上传信息
        if ($_FILES) {
            foreach ($_FILES as $key => &$val) {
                if ($val['size'] > $setting['maxSize']) {
                    throw new RuntimeException('上传文件过大！', 'SIZE_TRANSFINITE');
                }
                //上传文件类型
                if (!getimagesize($val['tmp_name'])) {
                    throw new RuntimeException('请选上传图像文件！', 'EXPECT_IMAGE');
                }
            }
        } else {
            throw new RuntimeException('请选择上传的文件！', 'LACK_FILE');
        }
        $Upload = new \Think\Upload($setting);
        $info = $Upload->upload($_FILES);
        if (empty($info)) {
            throw new RuntimeException('上传失败！', 'UPLOAD_ERROR');
        } else {
            //保存为附件
            $filesDatas = [];
            $result = [];
            foreach ($info as $key => &$val) {
                $filesData = [
                    'file_name' => $val['savename'],
                    'file_path' => $val['url'],
                    'file_ext' => $val['ext'],
                    'upload_time' => NOW_TIME,
                ];
                Hook::listen('create_attachment', $filesData);

                $filesDatas[] = $filesData;
                $result[] = [
                    'ext' => $val['ext'],
                    'url' => $val['url'],
                ];
            }
            $res = M('SysAttachment')->addAll($filesData);
            return $result;
        }
    }
}
