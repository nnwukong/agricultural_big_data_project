<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/6/10 11:58
// +----------------------------------------------------------------------
// | Readme: oss 客户端
// +----------------------------------------------------------------------

namespace Wkidt\FileManager\Client;

use app\common\exception\Exception;
use OSS\OssClient;
use Sts\Request\V20150401\AssumeRoleRequest;
use Wkidt\FileManager\Interfaces\FileManagerClient;

include_once __DIR__ . "/aliyun-php-sdk-sts/Sts/Request/V20150401/AssumeRoleRequest.php";

class Oss implements FileManagerClient
{
    /**
     * ossClient 实例
     *
     * @var null
     */
    protected $ossClient = null;

    /**
     * 配置主要是为了保证数组结构
     *
     * @var array
     */
    protected $config = [
        'access_key_id' => '',
        'access_key_secret' => '',
        'endpoint' => '',
        'bucket' => '',
        'role_arn' => '',
        'size' => 5242880,
        'ext' => 'jpg|jpeg|png|gif',
    ];

    /**
     * 实例化
     *
     * Oss constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->config = array_merge($this->config, $config);
        $this->ossClient = new OssClient($this->config['access_key_id'], $this->config['access_key_secret'], $this->config['endpoint']);
    }

    /**
     * 获取上传凭证
     *
     * @return string
     */
    public function getUploadAuth()
    {
        $iClientProfile = \DefaultProfile::getProfile("cn-hangzhou", $this->config['access_key_id'], $this->config['access_key_secret']);
        $client = new \DefaultAcsClient($iClientProfile);

        // 角色资源描述符，在RAM的控制台的资源详情页上可以获取
        $roleArn = $this->config['role_arn'];

        // 在扮演角色(AssumeRole)时，可以附加一个授权策略，进一步限制角色的权限；
        // 详情请参考《RAM使用指南》
        // 此授权策略表示读取所有OSS的只读权限
        $policy = <<<POLICY
{
  "Statement": [
    {
      "Action": "oss:*",
      "Effect": "Allow",
      "Resource": "*"
    }
  ],
  "Version": "1"
}
POLICY;

        $request = new AssumeRoleRequest();
        // RoleSessionName即临时身份的会话名称，用于区分不同的临时身份
        // 您可以使用您的客户的ID作为会话名称
        $request->setRoleSessionName("oss-sts");
        $request->setRoleArn($roleArn);
        $request->setPolicy($policy);
        $request->setDurationSeconds(3600);
        $response = $client->getAcsResponse($request);
        return $response->Credentials;
    }

    /**
     * 上传文件
     *
     * @param $filename
     * @param $filePath
     * @return null
     * @throws Exception
     */
    public function upload($filePath, $filename = null)
    {
        // 文件大小限制
        if ($this->config['size'] < abs(filesize($filePath))) {
            throw new Exception('上传文件过大');
        }

        // 文件后缀
        $ext = pathinfo(($filename ?: $filePath), PATHINFO_EXTENSION);
        if (!in_array(strtolower($ext), explode('|', $this->config['ext']))) {
            throw new Exception('不允许的上传文件类型');
        }

        // 默认文件md5作为文件名
        if (is_null($filename)) {
            $filename = md5_file($filePath) . '.' . $ext;
        }
        $result = $this->ossClient->uploadFile($this->config['bucket'], $filename, $filePath);
        if (false == $result) {
            return false;
        } else {
            $data = [
                'url' => isset($result['oss-request-url']) ? $result['oss-request-url'] : '',
                'key' => $filename,
            ];
            return $data;
        }
    }
}
