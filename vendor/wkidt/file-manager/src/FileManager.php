<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/6/10 11:53
// +----------------------------------------------------------------------
// | Readme: 实例管理器
// +----------------------------------------------------------------------

namespace Wkidt\FileManager;

use app\common\exception\Exception;
use think\Config;
use Wkidt\FileManager\Interfaces\FileManagerClient;

class FileManager
{

    /**
     * 客户端实例
     *
     * @var array
     */
    protected static $clients = [];

    /**
     * 创建上传客户端
     *
     * @param array $config
     * @return FileManagerClient
     * @throws Exception
     */
    public static function createClient($config = [])
    {
        $config = array_merge(Config::get('file'), $config);
        $key = md5('client_' . serialize($config));

        if (isset(self::$clients[$key])) {
            return self::$clients[$key];
        } else {
            $type = ucwords(isset($config['type']) ? $config['type'] : '');
            $class = "\\Wkidt\\FileManager\\Client\\{$type}";
            if (class_exists($class)) {
                $client = new $class($config);
                self::$clients[$key] = $client;
                return $client;
            } else {
                throw new Exception('上传驱动不存在');
            }
        }
    }
}

