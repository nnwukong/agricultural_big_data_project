import Main from '@/view/main'
import parentView from '@/components/parent-view'

/**
 * iview-admin中meta除了原生参数外可配置的参数:
 * meta: {
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  notCache: (false) 设为true后页面不会缓存
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 * }
 */

export default [
  {
    path: '/login',
    name: 'login',
    meta: {
      title: 'Login - 登录',
      hideInMenu: true
    },
    component: () => import('@/view/login/login.vue')
  },
  {
    path: '/',
    name: '_home',
    redirect: '/home',
    component: Main,
    meta: {
      hideInMenu: true,
      notCache: true
    },
    children: [
      {
        path: '/home',
        name: 'home',
        meta: {
          hideInMenu: true,
          title: '首页',
          notCache: true
        },
        component: () => import('@/view/single-page/home')
      }
    ]
  },
  {
    path: '/laiBinOCFarm',
    name: 'laiBinOCFarm',
    meta: {
      icon: 'md-menu',
      title: '来宾华侨农场'
    },
    component: Main,
    children: [
      {
        path: 'weatherStation1',
        name: 'weatherStation1',
        meta: {
          icon: 'md-funnel',
          title: '*1号气象站'
        },
        component: () => import('@/view/laiBinOCFarm/weatherStation1.vue')
      },
      {
        path: 'weatherStation2',
        name: 'weatherStation2',
        meta: {
          icon: 'md-funnel',
          title: '*2号气象站'
        },
        component: () => import('@/view/laiBinOCFarm/weatherStation2.vue')
      },
    ]
  },
  {
    path: '/FruitGrowerTreasure',
    name: 'FruitGrowerTreasure',
    meta: {
      icon: 'md-menu',
      title: '果农宝农业示范基地'
    },
    component: Main,
    children: [
      {
        path: 'weatherStation3',
        name: 'weatherStation3',
        meta: {
          icon: 'md-funnel',
          title: '*1号气象站'
        },
        component: () => import('@/view/FruitGrowerTreasure/weatherStation3.vue')
      },
      {
        path: 'weatherStation4',
        name: 'weatherStation4',
        meta: {
          icon: 'md-funnel',
          title: '*2号气象站'
        },
        component: () => import('@/view/FruitGrowerTreasure/weatherStation4.vue')
      },
    ]
  },
  {
    path: '/personal',
    name: 'personal',
    meta: {
      hideInMenu: true
    },
    component: Main,
    children: [
      {
        path: 'personal',
        name: 'personal',
        meta: {
          icon: 'md-person',
          title: '个人中心'
        },
        component: () => import('@/view/personal/personal.vue')
      }
    ]
  },
  {
    path: '/monitorPlatform',
    name: 'monitorPlatform',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/gxtcMP/monitorPlatform.vue')
  },
  {
    path: '/401',
    name: 'error_401',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/401.vue')
  },
  {
    path: '/500',
    name: 'error_500',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/500.vue')
  },
  {
    path: '*',
    name: 'error_404',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/view/error-page/404.vue')
  }
]
