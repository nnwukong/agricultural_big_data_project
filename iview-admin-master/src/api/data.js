import axios from '@/libs/api.request'

export const getTableData = () => {
  return axios.request({
    url: 'get_table_data',
    method: 'get'
  })
}

export const getAllTypeData = (params) => {
  return axios.request({
    url: 'GetReal',
    method: 'get',
    params: params,
    dataType:'JSONP',
  })
}
