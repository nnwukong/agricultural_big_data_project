import Vue from 'vue'
import Vuex from 'vuex'

import user from './module/user'
import app from './module/app'

Vue.use(Vuex)

export default new Vuex.Store({
  name: 'store',
  state: {
    //
    pageId: 1,
    mapName: '南宁'
  },
  mutations: {
    //
    getPageId (state, data) {
      state.pageId = data
    },
    getMapName (state, data) {
      state.mapName = data
    }
  },
  actions: {
    //
  },
  modules: {
    user,
    app
  }
})
