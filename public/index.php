<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/22 0022 12:20
// +----------------------------------------------------------------------
// | Readme: 入口文件
// +----------------------------------------------------------------------
$GLOBALS['HTTP_RAW_POST_DATA'] = file_get_contents("php://input");
// 根目录
define('ROOT_PATH', dirname(__DIR__) . '/');

// 定义应用目录
define('APP_PATH', ROOT_PATH . 'application/');

// thinkphp 入口
require __DIR__ . '/../vendor/topthink/framework/base.php';

// 执行应用
\Wkidt\think5\App::run()->send();
