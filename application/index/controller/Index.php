<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/24 0024 14:39
// +----------------------------------------------------------------------
// | Readme: 首页文件
// +----------------------------------------------------------------------

namespace app\index\Controller;

use think\Db;
use think\Session;
use Wkidt\think5\request\Request;

class Index extends \Wkidt\think5\Controller
{


    /**
     * 首页控制器
     *
     * @return \think\response\View
     */
    public function index()
    {
        return view();
    }


    public function login()
    {
        $username=Request::instance()->post('userName','1','trim');
        $password=Request::instance()->post('password','1','trim');
        if ('' == $username) {
            return ['code' => 'ERROR', 'info' =>  '请输入用户名'];
        }
        if ('' == $password) {
            return ['code' => 'ERROR', 'info' =>  '请输入密码'];
        }
        $admin=Db::name('user')
            ->where(['username'=>$username,'password'=>$password])
            ->field('password',true)
            ->find();
        if(!empty($admin)){
            $res['token']=$admin['username'];
            session('admin_id',$admin['id']);
            session('admin_info',$admin);
            return ['code' => 'success', 'info' => '登录成功','data'=>$res];
        }else{
            return ['code' => 'ERROR', 'info' =>  '登录失败'];
        }

        return $data;
    }

    public function getUserInfo(){
        $data['name'] = 'admin';
        $data['user_id'] = '2';
        $data['access'] = array('admin');
        $data['token'] = 'admin';
        $data['avator'] = 'https://avatars0.githubusercontent.com/u/20942571?s=460&v=4';

        return['data'=>$data];
    }


}
