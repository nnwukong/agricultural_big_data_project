<?php
// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team Tx <admin@wkidt.com> 2018/5/12 11:32
// +----------------------------------------------------------------------
// | Readme: 相片模型
// +----------------------------------------------------------------------


namespace  app\mobile\model;

use think\Db;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\DbException;
use think\Request;
use Wkidt\think5\model\Model;

class PhotoModel extends Model
{

    /**
     * 获取相片列
     * @param $shop_id
     * @return array
     * @throws \think\exception\PDOException
     */
    public  function  getPhotoList(){
        if (!empty(session('openid'))){
            $where['openid'] = session('openid');
        }else{
            $where['openid'] = 'owZDKwc1KASexGvRmZrprDcIAWkM';
        }
        $list=Db::name('photo')
            ->where($where)
            ->order(['id'=>'desc'])
            ->field('id as photo_id,title,status,addtimes')
            ->paginate();

        $data_list=$list->items();
        foreach ($data_list as $key => $v){
            $photo_thumb = Db::name('photo_src')->field('photo_src_type,photo_thumb,isbuy')->where('photo_id',$v['photo_id'])->select();
            $data_list[$key]['photo_thumb']=$photo_thumb;
        }
        if($data_list){
            return ['data' =>$data_list, 'page' => $list->getPageInfo()];
        }else{
            return null;
        }
    }

    /**
     * 获取照片详情
     * @param $id
     * @return array|null
     */
    public function  getPhotoInfo(){

        $id = Request::instance()->post('photo_id');

        if(empty($id)){
            return ['code' => 'ERROR', 'info' =>'没有相片id'];
        }
        $data['info']=Db::name('photo')
            ->where(['id'=>$id])
            ->field('openid,title,addtimes')
            ->find();

            $data['photo_src'] = Db::name('photo_src')
                ->alias('a')
                ->join('product b', 'a.photo_src_type = b.id')
                ->where('a.photo_id', $id)
                ->field('a.*,b.money,b.title')
                ->select();

        return $data;

    }

    /**
     * 获取照片底片详情
     * @param $id
     * @return array|null
     */
    public function  getPhotoInfoByid(){

        $photo_id = Request::instance()->post('photo_id');
        $photo_src_id = Request::instance()->post('photo_src_id');
        if(empty($photo_id||$photo_src_id)){
            return ['code' => 'ERROR', 'info' =>'没有相片id'];
        }
        $data['data']= Db::name('photo_src')->where('id',$photo_src_id)->find();

        return $data;

    }


    /**
     * 设置照片基本信息
     * @param $id
     * @param $thumb
     * @param $photo_src_zip
     * @param $datetime
     * @return array
     */
    public  function  setPhotoBase($id,$thumb,$photo_src_zip,$datetime){

        if(empty($id)){
            return ['code' => 'ERROR', 'info' =>'没有相片id'];
        }

        if($thumb){
         $data['thumb']=$thumb;
        }

        if($photo_src_zip){
            $data['photo_src_zip']=$photo_src_zip;
        }

        if($datetime){
            $data['datetime']=strtotime($datetime);
        }
        $data['status']=1;
        if(Db::name('photo')->where(['id'=>$id])->update($data)){
            return ['code' => 'SUCCESS', 'info' =>'设置成功！'];
        }else{
            return ['code' => 'ERROR', 'info' =>'设置失败'];
        }


    }


    /**
     * 上传照片底片
     * @param $id
     * @param $photo_id
     * @param $photo_src_type
     * @param $photo_thumb
     * @param $photo_src
     * @return array
     */
    public  function  uploadingPhoto($id,$photo_id,$photo_src_type,$photo_thumb,$photo_src){
        if(empty($photo_src_type)) {
            return ['code' => 'ERROR', 'info' =>'没有底片类型'];
        }
        if(empty($photo_id)) {
            return ['code' => 'ERROR', 'info' =>'没有照片id'];
        }

        $data['photo_id']=$photo_id;
        $data['photo_src_type']=$photo_src_type;
        $data['update_time']=time();
        if(!empty($photo_thumb)){
            $data['photo_thumb']=$photo_thumb;
        }
        if(!empty($photo_src)){
            $data['photo_src']=$photo_src;
        }

        if($id){
            $rs=Db::name('photo_src')->where(['photo_id'=>$id,'photo_id'=>$data['photo_id'],'photo_src_type'=>$data['photo_src_type']])->update($data);
        }else{

            if(Db::name('photo_src')->where(['photo_id'=>$data['photo_id'],'photo_src_type'=>$data['photo_src_type']])->count()){
                return ['code' => 'ERROR', 'info' =>'该照片已有此底片类型不能添加！'];
            }

            $rs=Db::name('photo_src')->insert($data);
        }


        if($rs){
            return ['code' => 'SUCCESS', 'info' =>'操作成功！'];
        }else{
            return ['code' => 'ERROR', 'info' =>'操作失败'];
        }


    }

}