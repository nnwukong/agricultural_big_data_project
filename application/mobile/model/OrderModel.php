<?php
// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team Tx <admin@wkidt.com> 2018/5/12 11:32
// +----------------------------------------------------------------------
// | Readme:订单模型
// +----------------------------------------------------------------------


namespace  app\mobile\model;

use think\Db;
use Wkidt\think5\model\Model;
use Wkidt\Number\Number;

class OrderModel extends Model
{

    /**
     * 添加订单
     * @param $post
     * @return array
     * @throws \think\exception\PDOException
     */
    public  function  addOrder($post){

        if(empty($post['photo_id'])){
            return ['code' => 'ERROR', 'info' =>'没有照片id'];
        }

        if(empty($post['product_id'])){
            return ['code' => 'ERROR', 'info' =>'没有产品数据'];
        }

        if(empty($post['realname'])){
            return ['code' => 'ERROR', 'info' =>'收件人姓名'];
        }

        if(empty($post['phone'])){
            return ['code' => 'ERROR', 'info' =>'没有收件人手机'];
        }

        if(empty($post['address'])){
            return ['code' => 'ERROR', 'info' =>'没有收件人地址'];
        }

        if(empty($post['num'])){
            $post['num']=1;
        }
        if(empty($post['remark'])){
            $post['remark']=1;
        }
        if (!empty(session('openid'))){
            $where['openid'] = session('openid');
        }else{
            $where['openid'] = 'owZDKwc1KASexGvRmZrprDcIAWkM';
        }

        //检查该用户是否已经购买过改产品
        $where['photo_id'] = $post['photo_id'];
        $where['product_id'] = $post['product_id'];
        $where['photo_src_id'] = $post['photo_src_id'];
        $where['pay_status'] = 2;
        $rs =DB::name('order')->where($where)->find();
        if($rs){
            return ['code' => 'ERROR', 'info' =>'用户已购买'];
        }

        //检查该用户是否有此照片
        $photo=DB::name('photo')->where(['id'=>$post['photo_id'],'openid'=>$where['openid']])->find();
        //echo Db::name('photo')->getLastSql();
        if(empty($photo)){
            return ['code' => 'ERROR', 'info' =>'该用户没有此照片'];
        }

        //获取产品属性
        $photo=DB::name('product')->where('id',$post['product_id'])->find();
        if(empty($photo)){
            return ['code' => 'ERROR', 'info' =>'改产品不存在'];
        }

        //echo Db::name('photo')->getLastSql();

        if(empty($photo)){
            return ['code' => 'ERROR', 'info' =>'该用户没有此照片'];
        }

        $data['order_sn']='order_'.date('YmdHis').mt_rand(1000, 9999);  //生成订单号
        $data['shop_id']=1; //店铺编号
        $data['openid'] = $where['openid']; //客户openid
        $data['photo_id'] = $post['photo_id'];  //关联相片ID  必填  1=底片购买
        $data['photo_src_id'] = $post['photo_src_id'];  //关联相片底片ID 必填
        $data['product_id'] = $post['product_id'];  //关联产品ID 必填
        $data['specification'] = $post['specification'];  //产品规格
        $data['amount'] = $post['num']; //购买数量
        $data['order_amount'] = $post['num']*$photo['money']; //购买数量
        $data['order_status'] = 0;   //订单状态 默认0未处理 1已完成 2 关闭 3 已删除
        $data['consignee'] = $post['realname'];  //客户姓名
        $data['consignee_phone'] = $post['phone'];  //手机
        $data['consignee_province'] = $post['consignee_province'];  //省
        $data['consignee_city'] = $post['consignee_city'];  //市
        $data['address'] = $post['address'];  //地址
        $data['pay_name'] ='wxpay';  //支付渠道
        $data['pay_status'] = 0;      //支付状态 默认0 未支付 1 已支付  2 已退款
        $data['remark'] = $post['remark']; //订单备注
        $data['create_time'] = time();  //订单添加时间

        if($rs = Db::name('order')->insertGetId($data)){
            $rs_data['order_id'] = $rs;
            $rs_data['order_sn'] = Db::name('order')->where('order_id',$rs)->value('order_sn');
            return ['code' => 'SUCCESS', 'info' =>'订单添加成功','data'=>$rs_data];
        }else{
            return false;
        }
    }
    /**
     * 添加虚拟产品订单（即不用发实体货物的订单）
     * @param $post
     * @return array
     * @throws \think\exception\PDOException
     */

    public function addVirtualOrder($post)
    {
        if(empty($post['photo_id'])){
            return ['code' => 'ERROR', 'info' =>'没有照片id'];
        }

        if(empty($post['product_id'])){
            return ['code' => 'ERROR', 'info' =>'没有订单类型'];
        }
        if(empty($post['amount'])){
            $post['amount']=1;
        }
        if(empty($post['remark'])){
            $post['remark']=1;
        }
        if(!empty(session('openid'))){
            $where['openid'] = session('openid');
        }else{
            $where['openid'] = 'owZDKwc1KASexGvRmZrprDcIAWkM';
        }
        $where['id'] = $post['photo_id'];
        //检查该用户是否已经购买过改产品

        //$rs=DB::name('order')->where(['photo_id'=>$post['photo_id'],'product_id'=>$post['product_id']])->find();
        //if(!empty($rs)){
            //return ['code' => 'ERROR', 'info' =>'你已经购买过该服务了'];
        //}
        //检查该用户是否有此照片
        $photo=DB::name('photo')->where($where)->find();
        //echo Db::name('photo')->getLastSql();
        if(empty($photo)){
            return ['code' => 'ERROR', 'info' =>'该用户没有此照片'];
        }

        //获取产品属性
        $photo=DB::name('product')->where('id',$post['product_id'])->find();
        if(empty($photo)){
            return ['code' => 'ERROR', 'info' =>'改产品不存在'];
        }

        //echo Db::name('photo')->getLastSql();

        if(empty($photo)){
            return ['code' => 'ERROR', 'info' =>'该用户没有此照片'];
        }

        $data['order_sn']='order_'.date('YmdHis').mt_rand(1000, 9999);  //生成订单号
        $data['shop_id']=1; //店铺编号
        $data['openid']=$where['openid']; //客户openid
        $data['photo_id']=$post['photo_id'];  //关联相片ID  必填  2:证件照底片；3:证件照蓝底；4:证件红底；5:证件照白底；6:证件照精修
        $data['photo_src_id']=$post['photo_src_id'];  //关联相片底片ID
        $data['product_id']=$post['product_id'];  //关联产品ID 必填
        $data['specification']='购买'.$photo['title'];  //产品规格
        $data['amount']=$post['amount']; //购买数量
        $data['order_amount']=$post['amount']*$photo['money']; //购买数量
        $data['order_status']=0;   //订单状态 默认0未处理 1已完成 2 关闭 3 已删除
        $data['pay_name']='wxpay';  //支付渠道
        $data['pay_status']=0;      //支付状态 默认0 未支付 1 已支付  2 已退款
        $data['remark']=$post['remark']; //订单备注
        $data['create_time']=time();  //订单添加时间

        if($rs = Db::name('order')->insertGetId($data)){
            return ['code' => 'SUCCESS', 'info' =>'添加订单成功！','order_id'=>$rs];
        }else{
            return false;
        }

    }
}