<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/9/28 10:16
// +----------------------------------------------------------------------
// | Readme: 配置模型
// +----------------------------------------------------------------------

namespace app\mobile\model;

use think\Cache;
use Wkidt\think5\model\Model;
use Wkidt\think5\request\Request;
use think\db;
use EasyWeChat\Factory;
//
class MemberModel extends Model
{
    /**
     *添加会员
     * 返回 会员的UID
     */
    public function addMember($data){
        $rs = Db::name('member')->insert($data);
        return $rs;

    }

    /**
     *查询会员
     * 返回 会员的UID
     */
    public function getMemberInfoByOpenid($openid){
        $rs=Db::name('member')->field('openid')->where(array('openid'=> $openid))->find();
        return $rs;
    }

    /**
     * 用户关注
     */
    public function addBySubscribe($open_id='',$scene_id=0){

        $app = new Application(config("EasyWechetConfig"));
        if(!empty($open_id)){

            $wx_user=$this->getMemberInfoByOpenid($open_id);

            if(empty($wx_user)){
                $users=$app->user->get($open_id);//获取用户信息
                if (!empty($users['nickname'])) {

                    //检查有没有上线id
                    if(!empty($scene_id)){
                        $invite=explode('_', $scene_id)['1'];
                        $data['invite_id'] =$invite ;
                        //$this->addMoney($invite , 50 ,"推广关注",'推广关注将激励');
                    }

                    $data = $users ;
                    $data['add_time'] = time();
                    $rs=$this->addMember($data);

                    if(!empty($rs)){
                        $uid=Db::name('member')->getLastInsID();
                        $url=$this->qrcode($uid); //生成关注二维码到到本地
                        if($url){
                            $filepath = $_SERVER['DOCUMENT_ROOT'].'/'.$url;
                            $material_info=$app->material->uploadImage($filepath,'image');//上传二维码到微信素
                            $material_info = json_decode($material_info,true);
                            if(!empty($material_info['media_id'])){
                                Db::name('member')->where(['uid'=>$uid])->update(['media_id'=>$material_info['media_id']]);
                            }
                        }

                    }
                }

            }

        }

    }


    /**
     * 生成带参数关注二维码
     * @return string
     */
    public  function  qrcode($uid){

        $data['action_name']='QR_LIMIT_STR_SCENE';
        $data['action_info']['scene']['scene_str']=$uid;
        $imageInfo=get_qrcode($data);

        $filename = "qrcode/".$data['action_info']['scene']['scene_str'].".jpg";
        $local_file = fopen($filename, 'w');
        if (false !== $local_file){
            if (false !== fwrite($local_file, $imageInfo)) {
                fclose($local_file);
            }
        }
        return $filename;
    }
}
