<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/24 0024 14:39
// +----------------------------------------------------------------------
// | Readme: 首页文件
// +----------------------------------------------------------------------

namespace app\mobile\Controller;
use app\common\Controller\Mobile;
use think\Db;
use think\Request;
use app\admin\model\PhotoModel;
use EasyWeChat\Factory;

class Index extends Mobile
{

    public function index()
    {
        return view();

    }

    /**
     * @return
     */
    public function test()
    {
        $order_id = Request::instance()->post('order_id');
        //var_export($order_id);
        $order_sn = Db::name('order')->where('order_id',$order_id)->find();
        if(!empty($order_sn['photo_id'])){
            $data = PhotoModel::instance()->updataSrcBuyStatus($order_sn['photo_id'],$order_sn['product_id']);
        }else{
            return false;
        }
        return $data;
    }

    /**
     * @return
     */
    public function jssdk()
    {
        $app = new Application(config("EasyWechetConfig"));
        $js = $app->js;
        $str = $js->config(array('onMenuShareQQ', 'onMenuShareWeibo'), true);
        var_export($str);
    }


    /**
     *  获取微信支付共享地址
     * param product_id
     *
     */
    public function getaddress()
    {

        $app = new Application(config("EasyWechetConfig"));
        $payment = $app->payment;
        if (empty($_GET['code'])) {
            $currentUrl = UrlHelper::current(); // 获取当前页 URL
            $response = $app->oauth->scopes(['snsapi_base'])->redirect($currentUrl);
            //var_export($response);
            return $response;

        }
        // 授权回来
        $oauthUser = $app->oauth->user();
        $token = $oauthUser->getAccessToken();
        $configForPickAddress = $payment->configForShareAddress($token);

    }


}