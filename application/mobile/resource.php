<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/6/3 18:22
// +----------------------------------------------------------------------
// | Readme: 资源配置文件
// +----------------------------------------------------------------------

return [
    '手机端' => [

        '用户' => [
            '会员信息@get_member_info' => ['/member/getMemberInfo' => ['mobile/Member/getMemberInfo', ['method' => 'post']],],
            '会员信息@get_user_info' => ['/member/getUserInfo' => ['mobile/Member/getUserInfo', ['method' => 'post']],],
            '会员退出登录@member_logout' => ['/member/logout' => ['mobile/Member/Logout', ['method' => 'post']],],
            '获取会员数据@getDataInfo' => ['/getDataInfo' => ['mobile/Index/getDataInfo', ['method' => 'post']],]
        ],
        '帐户' => [
            '获取帐户信息@getAccountInfo' => ['/member/getAccountInfo' => ['mobile/Account/getAccountInfo', ['method' => 'get']],],
            '获取帐户记录@getAccountList' => ['/member/getAccountList' => ['mobile/Account/getAccountList', ['method' => 'get']],],
            '获取帐户记录@getScoreList' => ['/member/getScoreList' => ['mobile/Account/getScoreList', ['method' => 'get']],],

        ],

        '订单' => [
            '订单列表@get_order_list' => ['/member/getOrderList' => ['mobile/Order/getOrderList', ['method' => 'post']],],
            '订单统计@get_order_conut' => ['/member/getOrderCount' => ['mobile/Order/getOrderCount', ['method' => 'post']],],
            '订单详情@get_order_info' => ['/member/getOrderInfo' => ['mobile/Order/getOrderInfo', ['method' => 'post']],],
            '提交订单@add_order' => ['/member/addOrder' => ['mobile/Order/addOrder', ['method' => 'post']],],
            '提交虚拟产品订单@addaddVirtualOrder' => ['/member/addVirtualOrder' => ['mobile/Order/addVirtualOrder', ['method' => 'post']],],
            '提交订单@wxpay' => ['/member/wxpay' => ['mobile/Order/wxpay', ['method' => 'post']],],
            '查询产品信息@getProductInfo' => ['/member/getProductInfo' => ['mobile/Order/getProductInfo', ['method' => 'post']],]

        ],

        '相片' => [
            '相片列表@mobile/get_photo_list' => ['/member/getPhotoList' => ['mobile/Photo/getPhotoList', ['method' => 'get']],],
            '相片相片详情@mobile/get_photo_info' => ['/member/getPhotoInfo' => ['mobile/Photo/getPhotoInfo', ['method' => 'post']],],
            '相片相片底片详情@mobile/get_photo_info_by_id' => ['/member/getPhotoInfoByid' => ['mobile/Photo/getPhotoInfoByid', ['method' => 'post']],],
            '打印相片@print_photo' => ['/member/printPhoto' => ['mobile/Photo/printPhoto', ['method' => 'post']],],
            '下载底片@download_src' => ['/member/downlaodSrc' => ['mobile/Photo/downlaodSrc', ['method' => 'post']],]

        ],

        '卡包' => [
            '卡券列表@get_coupons_list' => ['/member/getCouponsList' => ['mobile/Coupons/getCouponsList', ['method' => 'post']],],
            '卡券详情@getCouponsInfo' => ['/member/getCouponsInfo' => ['mobile/Coupons/getCouponsInfo', ['method' => 'post']],],
            '卡券使用记录@getCouponsLog' => ['/member/getCouponsLog' => ['mobile/Coupons/getCouponsLog', ['method' => 'post']],]
        ],

        '预约管理' => [
            '获取预约项目列表@mobile/getAppointmentList' => ['mobile/getAppointmentList' => ['mobile/Appointment/getAppointmentList', ['method' => 'post']],],
            '获取预约时间@mobile/admin_get_appointment_list' => ['mobile/get_appointment_date' => ['mobile/Appointment/getAppointmentDate', ['method' => 'get']],],
            '获取预约时间的点位@mobile/getAppointmentDateDot' => ['mobile/get_appointment_date_dot' => ['mobile/Appointment/getAppointmentDateDot', ['method' => 'get']],],
            '用户预约项目@mobile/addAppointment' => ['mobile/add_appointment' => ['mobile/Appointment/addAppointment', ['method' => 'post']],],
            '用户获取预约项目@mobile/getMemberAppointment' => ['mobile/get_member_appointment' => ['mobile/Appointment/getMemberAppointment', ['method' => 'get']],],
            '用户取消预约@mobile/cancelAppointment' => ['mobile/cancel_appointment' => ['mobile/Appointment/cancelAppointment', ['method' => 'post']],],
        ],

        '微信支付' => [
            '查询微信共享地址@getaddress' => ['/member/getaddress' => ['mobile/index/getaddress', ['method' => 'get']],],
            '获取JSSDK@mobile/wxjssdk' => ['/member/jssdk' => ['mobile/index/jssdk', ['method' => 'get']],]
        ],

        '测试接口' => [
            '管理员登录@mtest2' => ['/member/posttest' => ['mobile/Index/test', ['method' => 'post']],],
            '管理员登录@mTest' => ['/member/gettest' => ['mobile/Index/test', ['method' => 'get']],]
        ]

    ]



];


