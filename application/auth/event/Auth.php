<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team 73504 <admin@wkidt.com> 2018/3/14 14:25
// +----------------------------------------------------------------------
// | Readme: 文件说明
// +----------------------------------------------------------------------

namespace app\auth\event;

use app\system\interfaces\AuthServiceProvider;
use app\system\logic\Event;

class Auth implements AuthServiceProvider
{

    /**
     * 检查是否允许对 $code 的访问
     *
     * @param $code string 请求对应的资源code
     * @return bool 是否有权限访问 true 有 false 无
     */
    public function check($code)
    {

        return true;
    }

    /**
     * 当前访问者是否已登录
     *
     * @return bool
     */
    public function hasAuth()
    {
        return true;
    }

    /**
     * 注册授权提供者
     *
     * @param Event $event
     * @param $am
     */
    public function loadAuthServiceProvider(Event $event, $am)
    {

        $am->addAuthServiceProvider($this);
    }
}
