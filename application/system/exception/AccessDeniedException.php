<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/5/14 15:14
// +----------------------------------------------------------------------
// | Readme: 拒绝访问异常
// +----------------------------------------------------------------------

namespace app\system\exception;

use think\Lang;
use Wkidt\think5\exception\Exception;

class AccessDeniedException extends Exception
{

    /**
     * 异常码
     *
     * @var string
     */
    protected $code = 'access_denied';

    /**
     * 初始化
     *
     * AccessDeniedException constructor.
     * @param string $message
     * @param string $code
     * @param \Throwable|null $previous
     */
    public function __construct($message = '', $code = '', \Throwable $previous = null)
    {
        $this->message = Lang::get('access_denied');
        parent::__construct($message, $code, $previous);
    }
}
