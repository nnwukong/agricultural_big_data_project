<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/6/9 19:03
// +----------------------------------------------------------------------
// | Readme: 凭证无效异常
// +----------------------------------------------------------------------

namespace app\system\exception;

use think\Lang;
use Wkidt\think5\exception\Exception;

class AuthInvalidException extends Exception
{

    /**
     * 异常码
     *
     * @var string
     */
    protected $code = 'auth_invalid';

    /**
     * 初始化
     *
     * AuthInvalidException constructor.
     * @param string $message
     * @param string $code
     * @param \Throwable|null $previous
     */
    public function __construct($message = '', $code = '', \Throwable $previous = null)
    {

        $this->message = Lang::get('auth_invalid');
        parent::__construct($message, $code, $previous);
    }

}
