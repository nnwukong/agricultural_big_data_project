<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team 73504 <admin@wkidt.com> 2017/11/13 {TIME}
// +----------------------------------------------------------------------
// | Readme: 语音包加载类
// +----------------------------------------------------------------------

namespace app\system\behavior;

use think\App;
use think\Config;

class Lang
{

    /**
     * 语言文件目录
     *
     * @var array
     */
    protected static $dirs = [];

    /**
     * 缓存文件路径
     *
     * @var string
     */
    protected $cacheFile = '';

    /**
     * 当前语言
     *
     * @var string
     */
    protected $lang = '';

    /**
     * 初始化
     *
     * Lang constructor.
     */
    public function __construct()
    {
        $this->lang = Config::get('default_lang');
        $this->cacheFile = RUNTIME_PATH . $this->lang . ".lock";
    }

    /**
     * 加载语言包
     *
     */
    public function appInit()
    {
        if (is_file($this->cacheFile)) {
            $cache = explode(',', file_get_contents($this->cacheFile), 2);
            if (!App::$debug || $cache[0] === $this->getLangFilesKey()) {
                $langData = isset($cache[1]) ? unserialize($cache[1]) : [];
            }
        }
        if (!isset($langData) || false == $langData) {
            $langData = $this->reCache();
        }
        \think\Lang::set($langData);
    }

    /**
     * 重新创建缓存
     *
     */
    protected function reCache()
    {
        $lang = [];
        foreach ($this->getAllLangFiles() as $file) {
            $data = include $file;
            $lang = array_merge($lang, (array)$data);
        }

        file_put_contents($this->cacheFile, $this->getLangFilesKey() . ',' . serialize($lang));
        return $lang;
    }

    /**
     * 获取语言包key
     *
     * @return string
     */
    protected function getLangFilesKey()
    {
        static $key = null;
        if (false == $key) {
            $files = $this->getAllLangFiles();
            $fileInfo = '';
            foreach ($files as $file) {
                $fileInfo .= $file . filemtime($file);
            }
            $key = md5($fileInfo);
        }
        return $key;
    }

    /**
     * 获取所有语音包文件
     *
     * @return array
     */
    protected function getAllLangFiles()
    {
        static $files = [];
        if (false == $files) {
            $dirs = array_merge(self::$dirs, (array)Config::get('lang_dir'), $this->getModuleLangDirs());
            foreach ($dirs as $dir) {
                $filePath = "{$dir}{$this->lang}.php";
                if (is_file($filePath)) {
                    $files[] = $filePath;
                }
            }
        }
        return $files;
    }

    /**
     * 获取所有模块资源文件
     *
     * @return array
     */
    protected function getModuleLangDirs()
    {
        $files = scandir(APP_PATH);
        foreach ($files as $dir) {
            $dir = APP_PATH . "{$dir}/lang/";
            if (is_dir($dir)) {
                $dirs[] = $dir;
            }
        }
        return isset($dirs) ? $dirs : [];
    }

}
