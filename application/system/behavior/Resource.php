<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/5/14 14:59
// +----------------------------------------------------------------------
// | Readme: 资源
// +----------------------------------------------------------------------

namespace app\system\behavior;

use app\system\exception\AccessDeniedException;
use app\system\logic\Resource as ResourceLogic;
use think\Request;

class Resource
{
    /**
     * 应用初始化导入路由
     *
     */
    public function appInit()
    {
        // 检测更新资源
        $resource = ResourceLogic::instance();
        $resource->isUpdate() && $resource->updateAll();

        // 资源转换为路由
        $resource->importToRoute();
    }

    /**
     * 资源访问控制
     *
     * @param $dispatch
     * @throws AccessDeniedException
     */
    public function appBegin($dispatch)
    {
        $routeInfo = Request::instance()->routeInfo();
        $code = isset($routeInfo['option']['code']) ? $routeInfo['option']['code'] : '';

        ResourceLogic::instance()->accessControl($code);
    }

}
