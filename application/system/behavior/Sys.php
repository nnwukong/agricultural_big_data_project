<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/4/26 21:44
// +----------------------------------------------------------------------
// | Readme: 系统行为
// +----------------------------------------------------------------------

namespace app\system\behavior;

use think\Config;
use Wkidt\think5\request\Request;

class Sys
{
    /**
     * 应用开始重定向模板路径
     *
     * @param $dispatch
     */
    public function appBegin(&$dispatch)
    {
        // 重新定位模板文件
        $viewBasePath = Config::get('template.view_base');
        if (empty($viewBasePath)) {
            Config::set('template.view_base', ROOT_PATH . 'template' . DS . $this->parseTheme() . DS);
        }
    }

    /**
     * 初始化
     *
     */
    public function appInit()
    {
        // 客户端类型
        $this->clientType();
    }

    /**
     * 定义客户端类型
     *
     */
    private function clientType()
    {
        define('__CLIENT_TYPE__', Request::instance()->getClientType());
        define('IS_IOS', (Request::CLIENT_IOS == __CLIENT_TYPE__ ? true : false));
        define('IS_ANDROID', (Request::CLIENT_ANDROID == __CLIENT_TYPE__ ? true : false));
        define('IS_WEIXIN', (Request::CLIENT_WEIXIN == __CLIENT_TYPE__ ? true : false));
        define('IS_MOBILE', (Request::CLIENT_MOBILE == __CLIENT_TYPE__ ? true : false));
        define('IS_PC', (Request::CLIENT_PC == __CLIENT_TYPE__ ? true : false));
        define('IS_APP', (IS_IOS || IS_ANDROID) ? true : false);
    }

    /**
     * 分析主题
     *
     * @return string
     */
    private function parseTheme()
    {
        //$theme = cookie('template_theme');
        //if (empty($theme)) {
            $theme = (IS_MOBILE || IS_APP || $this->isMobile()) ? Config::get('default_theme.mobile') : Config::get('default_theme.pc');
          //  cookie('template_theme', $theme);
        //}
        return $theme;
    }


    private  function isMobile() {
        // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
        if (isset($_SERVER['HTTP_X_WAP_PROFILE'])) {
            return true;
        }
        // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
        if (isset($_SERVER['HTTP_VIA'])) {
            // 找不到为flase,否则为true
            return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
        }
        // 脑残法，判断手机发送的客户端标志,兼容性有待提高。其中'MicroMessenger'是电脑微信
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $clientkeywords = array('nokia','sony','ericsson','mot','samsung','htc','sgh','lg','sharp','sie-','philips','panasonic','alcatel','lenovo','iphone','ipod','blackberry','meizu','android','netfront','symbian','ucweb','windowsce','palm','operamini','operamobi','openwave','nexusone','cldc','midp','wap','mobile','MicroMessenger');
            // 从HTTP_USER_AGENT中查找手机浏览器的关键字
            if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
                return true;
            }
        }
        // 协议法，因为有可能不准确，放到最后判断
        if (isset ($_SERVER['HTTP_ACCEPT'])) {
            // 如果只支持wml并且不支持html那一定是移动设备
            // 如果支持wml和html但是wml在html之前则是移动设备
            if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
                return true;
            }
        }
        return false;
    }

}
