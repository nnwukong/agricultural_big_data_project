<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/10/5 10:42
// +----------------------------------------------------------------------
// | Readme: 系统事件转发
// +----------------------------------------------------------------------

namespace app\system\behavior;

class Event
{

    /**
     * 应用初始化
     *
     */
    public function appInit()
    {
        \app\system\logic\Event::load();

        \app\system\logic\Event::trigger('app_init');
    }

    /**
     * 应用开始
     *
     * @param $dispatch
     */
    public function appBegin(&$dispatch)
    {

        \app\system\logic\Event::trigger('app_begin', $dispatch);
    }

    /**
     * 应用结束
     *
     * @param $response
     */
    public function appEnd(&$response)
    {

        \app\system\logic\Event::trigger('app_end', $response);
    }

    /**
     * 模块初始化
     *
     * @param $request
     */
    public function moduleInit(&$request)
    {

        \app\system\logic\Event::trigger('module_init', $request);
    }

    /**
     * 方法开始事件
     *
     * @param $call
     */
    public function actionBegin(&$call)
    {

        \app\system\logic\Event::trigger('action_begin', $call);
    }

    /**
     * 写日志事件
     *
     * @param $log
     */
    public function logWrite(&$log)
    {

        \app\system\logic\Event::trigger('log_write', $log);
    }

    /**
     * 保存调试信息事件
     *
     * @param $log
     */
    public function logWriteDone(&$log)
    {

        \app\system\logic\Event::trigger('log_write_done', $log);
    }

    /**
     * 响应发送事件
     *
     * @param $response
     */
    public function responseSend(&$response)
    {

        \app\system\logic\Event::trigger('response_send', $response);
    }

    /**
     * 响应发送结束事件
     *
     * @param $response
     */
    public function responseEnd(&$response)
    {

        \app\system\logic\Event::trigger('response_end', $response);
    }

    /**
     * 内容过滤事件
     *
     * @param $content
     */
    public function viewFilter(&$content)
    {

        \app\system\logic\Event::trigger('view_filter', $content);
    }
}
