<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/9/24 9:47
// +----------------------------------------------------------------------
// | Readme: 授权器
// +----------------------------------------------------------------------

namespace app\system\interfaces;

interface AuthServiceProvider
{

    /**
     * 检测是否存在权限
     *
     * @param $code
     * @return bool
     */
    public function check($code);

    /**
     * 是否存在凭证
     *
     * @return bool
     */
    public function hasAuth();
}
