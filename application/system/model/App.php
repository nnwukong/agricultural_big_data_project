<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/6/2 17:18
// +----------------------------------------------------------------------
// | Readme: 应用模型
// +----------------------------------------------------------------------

namespace app\system\model;

use Wkidt\think5\model\Model;
use traits\model\SoftDelete;

class App extends Model
{
    use SoftDelete;

    protected $name = 'sys_app';

    /**
     * 时间自动完成
     *
     * @var bool
     */
    protected $autoWriteTimestamp = true;

    /**
     * 获取应用的uri
     *
     * @param $key
     * @return string|false
     */
    public function getUri($key)
    {
        $app = $this->where('key', $key)->find();

        if ($app) {
            $uri = ($app['https'] ? 'https' : 'http') . "://{$app['host']}";
            return $uri;
        } else {
            return false;
        }
    }
}
