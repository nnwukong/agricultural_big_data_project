<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/9/28 10:16
// +----------------------------------------------------------------------
// | Readme: 配置模型
// +----------------------------------------------------------------------

namespace app\system\model;

use think\Cache;
use Wkidt\think5\model\Model;

class Config extends Model
{

    protected $name = 'sys_config';

    /**
     * 获取所有的配置项
     *
     * @return array
     */
    public function getAll()
    {
        if (false === $config = Cache::get('sys_config')) {
            $list = $this->field(['range', 'key', 'value'])->select();
            $config = [];
            foreach ($list as $key => $val) {
                $config[$val['range']][$val['key']] = $val['value'];
            }

            Cache::tag('config')->set('sys_config', $config);
        }
        return $config;
    }

    /**
     * 加载配置
     *
     */
    public function appInit()
    {
        // 获取设置表的数据导入系统配置
        $config = Config::instance()->getAll();
        foreach ($config as $key => $value) {
            if (false == $key) {
                \think\Config::set($value);
            } else {
                \think\Config::set($value, null, $key);
            }
        }
    }
}
