<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/5/14 9:39
// +----------------------------------------------------------------------
// | Readme: 字典模型
// +----------------------------------------------------------------------

namespace app\system\model;

use Wkidt\think5\model\Model;
use traits\model\SoftDelete;

class Dictionary extends Model
{

    use SoftDelete;

    /**
     * 时间自动完成
     *
     * @var bool
     */
    protected $autoWriteTimestamp = true;

    /**
     * 表名称
     *
     * @var string
     */
    protected $name = 'sys_dictionary';

}