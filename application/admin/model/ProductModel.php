<?php
// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team Tx <admin@wkidt.com> 2018/5/12 11:32
// +----------------------------------------------------------------------
// | Readme: 相片模型
// +----------------------------------------------------------------------


namespace  app\admin\model;

use think\Db;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\DbException;
use think\Session;
use Wkidt\think5\model\Model;
use app\admin\model\CouponesModel;

class ProductModel extends Model
{

    /**
     * 获取商品列表
     * @return array|null
     */
    public  function  getProductList($query){
        if(!array_key_exists("status", $query)){
            $query['status'] = 1 ;
            }

        if($query['key']=='all') {
            try {
                $data['data'] = Db::name('Product')
                    ->where(array('status' => $query['status']))
                    ->order(['id' => 'desc'])
                    ->select();
            } catch (DataNotFoundException $e) {
            } catch (ModelNotFoundException $e) {
            } catch (DbException $e) {
            }
            return $data ? $data : null;
        }else{
            $list=Db::name('Product')
                ->where('type',$query['type'])
                ->where('status',$query['status'])
                ->order(['id'=>'desc'])
                ->paginate();

            $data=$list->items();
            return $data ? ['data' => $data, 'page' => $list->getPageInfo()] : ['code' => 'error', 'info'=>'无数据', 'data'=>[]];
        }

    }

    /**
     * 添加商品
     * @return array|null
     */
    public  function  addProduct($data){
        $data['price'] = $data['price']*100 ;
        $data['weekend_price'] = $data['weekend_price']*100 ;
        $data['addtimes'] = time();
        $res = Db::name('product')->strict(true)->insertGetId($data);
        return ['data'=>$res];
    }

    /**
     * 更新商品
     * @return array|null
     */
    public  function  updateProduct($data){
        $data['price'] = $data['price']*100 ;
        $data['weekend_price'] = $data['weekend_price']*100 ;
        $data['updatetimes'] = time();
        $res = Db::name('product')->where('id',$data['id'])->strict(true)->update($data);
        return ['data'=>$res];
    }
}