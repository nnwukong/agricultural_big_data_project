<?php
// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team Tx <admin@wkidt.com> 2018/5/12 11:32
// +----------------------------------------------------------------------
// | Readme: 日志模型
// +----------------------------------------------------------------------


namespace  app\admin\model;

use think\Db;
use think\Session;
use Wkidt\think5\model\Model;
use think\Request;

class LogModel extends Model
{




    public  function  addMemberLog($Log){
        $arr = session('admin_info');
        $Log['admin'] = $arr['username'];
        $Log['addTime'] = time();
        Db::name('member_log')->insert($Log);
        return true;
    }



    /**
     * 添加日志
     * @param $title
     * @param $username
     * @return int
     */
    public  function  addLog($username, $title){

        if(empty($username)){
            $data['username'] = Session::get('username');
        }else{
            $data['username'] = trim($username);
        }

        if(empty($title)){
            $data['title'] = '未定义';
        }else{
            $data['title'] = trim($title);
        }

        $data['addtimes'] = time();

        /** @var TYPE_NAME $rs */
        $rs = Db::name('sys_log')->insert($data);
        $log_Id = Db::name('sys_log')->getLastInsID();

        return $log_Id;
    }

    /**
     * 日志列表
     * @param $username
     * @return object
     */
    public  function  getLogList($query){
        //todo:需要优化自动带分页数据
        $rs['data'] = Db::name('member_log')->where($query)->select();

        return $rs;
    }

}