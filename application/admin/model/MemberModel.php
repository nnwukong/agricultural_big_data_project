<?php
// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team Tx <admin@wkidt.com> 2018/5/12 11:32
// +----------------------------------------------------------------------
// | Readme: 管理员模型
// +----------------------------------------------------------------------


namespace  app\admin\model;

use think\Db;
use Wkidt\think5\model\Model;
use app\admin\model\LogModel;

class MemberModel extends Model
{

    /**
     * 表名称
     *
     * @var string
     */
    protected $name = 'member';

    /**
     * 获取会员列表
     * @return array|null
     * @throws \think\exception\DbException
     */
    public  function  getMemberList(){
        $list=$this
            ->order(['uid'=>'asc'])
            ->field('*')
            ->paginate();

        $data=$list->items();
        if($data){
            return ['data' =>$data, 'page' => $list->getPageInfo()];
        }else{
            return null;
        }
    }

    /**
     * 获取会员信息
     * @return array|null
     * @throws \think\exception\DbException
     */
    public function getMemberInfo($query){

        $data = $this
            ->alias('mem')
            ->join('wk_member_group group','mem.member_group=group.id','left')
            ->where($query)
            ->field('mem.*,group.title as member_group_title,group.discount')
            ->find();
        if($data){
            return $data;
        }else{
            return null;
        }
    }

    /**
     * 更新会员信息
     * @return mixed
     */
    public  function  setMemberInfo($data){

        $data['lastUpdateTime'] = time();
        $res = Db::name('member')->where('uid',$data['uid'])->update($data);
        // 写日志
        $str='';
        foreach($data as $item){
            $str=$str.'|'.$item;
        }
        $Log['title'] = "更新会员卡信息".$str;
        $Log['uid'] = $data['uid'];
        LogModel::instance()->addMemberLog($Log);
        if ($res){
            return true;
        }else{
            return false;
        }


    }

}