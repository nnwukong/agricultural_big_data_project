<?php
// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team Tx <admin@wkidt.com> 2018/5/12 11:32
// +----------------------------------------------------------------------
// | Readme: 相片模型
// +----------------------------------------------------------------------


namespace  app\admin\model;

use think\Db;
use think\Session;
use Wkidt\think5\model\Model;
use app\admin\model\CouponesModel;

class PhotoModel extends Model
{

    /**
     * 添加相片
     * @return array|null
     */
    public  function  addPhoto($data,$type){
        $type?$type:1;
        $photo['title'] = '伯牙奖拍摄';
        $photo['openid'] = $data->FromUserName;
        $photo['card_id'] = $data->CardId;
        $photo['usercardcode'] = $data->UserCardCode;
        $photo['verification_name'] = $data->StaffOpenId;
        $photo['thumb'] = 'https://xiaomeihao.oss-cn-shenzhen.aliyuncs.com/noimg.png';
        $photo['addtimes'] = time();
        $photo['product_type'] = 1; //默认产品类型为证件照
        $photo['remark'] = $data->OuterStr;

        $rs = Db::name('photo')->where(array('card_id'=>$photo['card_id'],'usercardcode'=>$photo['usercardcode']))->find();
        if($rs){
            return ['code' => 'ERROR', 'info' =>'数据已经存在'];
        }

        /****
         * 开始事务
         *
         * 1、添加相片订单
         * 2、生成相片的底片记录
         */

        Db::startTrans();
        try{
            //添加相片记录
            $rs = DB::name('photo')->insertGetId($photo);
            //添加相片底片记录
            if($rs){
                $photo_src_type = Db::name('photo_src_type')->select();
                foreach ($photo_src_type as $k=>$v){
                    $mydata[$k]['photo_id']=$rs;
                    $mydata[$k]['photo_src_type']=$v['key'];
                    $mydata[$k]['photo_thumb']='https://xiaomeihao.oss-cn-shenzhen.aliyuncs.com/noimg.png';
                    $mydata[$k]['photo_src']='https://xiaomeihao.oss-cn-shenzhen.aliyuncs.com/noimg.png';
                    $mydata[$k]['update_time']=time();
                }
            }
            Db::name('photo_src')->insertAll($mydata);

            //更新普通白色底片为默认购买
            $where['photo_id'] = $rs;
            $where['photo_src_type'] = 'id_photo_white_src';
            Db::name('photo_src')->where($where)->update(['isbuy'=>1]);
            //提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
        }

        return $rs;
    }

    /**
     * 获取相片列表
     * @return array|null
     */
    public  function  getPhotoList(){
        $list=Db::name('photo')
            ->order(['id'=>'desc'])
            ->field('id,title,card_id,usercardcode,openid,product_type,status,addtimes,verification_name')
            ->paginate();

        $data=$list->items();
        if($data){
            foreach($data as &$v){
                $v['member_info']=Db::name('member')->field('nickname,headimgurl,openid')->where(['openid'=>$v['openid']])->find();
                $v['verification_name']=Db::name('member')->where(['openid'=>$v['verification_name']])->value('nickname');
                $v['product_name']='证件照';
            }
            return ['data' =>$data, 'page' => $list->getPageInfo()];
        }else{
            return null;
        }
    }

    /**
     * 获取照片详情
     * @param $id
     * @return array|null
     */
    public function  getPhotoInfo($id){

        if(empty($id)){
            return ['code' => 'ERROR', 'info' =>'没有相片id'];
        }
        $info=Db::name('photo')
            ->where(['id'=>$id])
            ->find();

        if($info){
            $info['member_info']=Db::name('member')->field('nickname,headimgurl')->where(['openid'=>$info['openid']])->find();
            $info['product_name']='证件照';//todo 根据product_type 类型找项目类型名称
            $data['info']=$info;

            //$photo_src_type=Db::name('photo_src_type')->select();
            $photo_src=Db::name('photo_src')
                     ->where(['photo_id'=>$info['id']])
                     ->select();
            $photo=[];
            /*
            foreach ($photo_src_type as $vv){
                foreach ($photo_src as &$vo){
                    if($vv['key']==$vo['photo_src_type']){
                        $photo[$vv['key']]=$vo;
                        unset($vo['key']);
                    }

                }
                if(empty($photo[$vv['key']])){
                    $photo[$vv['key']]='';
                }

            }*/
            $data['photo_src']=$photo_src;
            return ['data' =>$data];
        }else{
            return null;
        }

    }


    /**
     * 设置照片基本信息
     * @param $id
     * @param $thumb
     * @param $photo_src_zip
     * @param $datetime
     * @return array
     */
    public  function  setPhotoBase($id,$thumb,$photo_src_zip,$datetime){

        if(empty($id)){
            return ['code' => 'ERROR', 'info' =>'没有相片id'];
        }

        if($thumb){
         $data['thumb']=$thumb;
        }

        if($photo_src_zip){
            $data['photo_src_zip']=$photo_src_zip;
        }

        if($datetime){
            $data['datetime']=strtotime($datetime);
        }
        $data['status']=1;
        if(Db::name('photo')->where(['id'=>$id])->update($data)){
            return ['code' => 'SUCCESS', 'info' =>'设置成功！'];
        }else{
            return ['code' => 'ERROR', 'info' =>'设置失败'];
        }


    }


    /**
     * 上传照片底片
     * @param $id 底片ID
     * @param $photo_相片ID
     * @param $photo_src_type 底片名称
     * @param $photo_src 底片地址
     * @return array
     */
    public  function  uploadingPhoto($id,$photo_id,$photo_src_type,$photo_src,$isbuy){
        if(empty($photo_src_type)) {
            $photo_src_type = '未命名';
        }
        if(empty($photo_id)) {
            return ['code' => 'ERROR', 'info' =>'没有照片id'];
        }

        $data['photo_id']=$photo_id;
        $data['photo_src_type']=$photo_src_type;
        $data['update_time']=time();
        $data['isbuy']=$isbuy;
        if(!empty($photo_thumb)){
            $data['photo_thumb']=$photo_thumb;
        }
        if(!empty($photo_src)){
            $data['photo_src']=$photo_src;
        }
        //$str = var_export($data,true);
        //file_put_contents('log.txt',$str,FILE_APPEND);
        if($id){
            $rs=Db::name('photo_src')->where(['id'=>$id])->update($data);

        }else{
            $rs=Db::name('photo_src')->insert($data);

        }

        if($rs){
            return ['code' => 'SUCCESS', 'info' =>'操作成功！'];
        }else{
            return ['code' => 'ERROR', 'info' =>'操作失败'];
        }


    }

    /**
     * 更新底片购买状态
     * @param $photo_id
     * @param $product_id
     * @return array
     */
    public  function  updataSrcBuyStatus($photo_id,$product_id){
        //根据购买的产品订单更新相片的购买状态
        switch ($product_id){
            //如果是购买相片底片源码的，如果购买支付已经成功，则更新改相片的底片记录为已购买
            case 2:
                $id_photo_src['photo_id']=$photo_id;
                $id_photo_src['photo_src_type']='id_photo_src';
                return Db::name('photo_src')->where($id_photo_src)->update(['isbuy' => 1]);
                break;
            //如果是购买蓝色底片，
            //1、直接更id_photo_blue_src的购买状态
            case 3:
                $id_photo_blue_src['photo_id']=$photo_id;
                $id_photo_blue_src['photo_src_type']='id_photo_blue_src';
                return Db::name('photo_src')->where($id_photo_blue_src)->update(['isbuy' => 1]);
                break;
            //如果是购买红色底片，
            //1、直接更id_photo_red_src的购买状态
            case 4:
                $id_photo_red_src['photo_id']=$photo_id;
                $id_photo_red_src['photo_src_type']='id_photo_red_src';
                return Db::name('photo_src')->where($id_photo_red_src)->update(['isbuy' => 1]);
                break;
            //如果是购买红色底片，
            //1、直接更id_photo_white_src的购买状态
            case 5:
                $id_photo_white_src['photo_id']=$photo_id;
                $id_photo_white_src['photo_src_type']='id_photo_white_src';
                return Db::name('photo_src')->where($id_photo_white_src)->update(['isbuy' => 1]);
                break;
            //如果是购买精修底片，
            //1、直接更id_photo_white_src的购买状态
            //2、查看
            case 6:
                $where['photo_id']=$photo_id;
                return Db::name('photo_src')->where($where)->update(['isbuy' => 1]);
                break;
            case 7:
                $where['photo_id']=$photo_id;
                return Db::name('photo_src')->where($where)->update(['isbuy' => 1]);
                break;
            default :
                return true ;

        }

    }

    /**
     * 删除照片底片
     * @return array
     */
    public  function  delPhotoSrc($id){
        return Db::name('photo_src')->where('id',$id)->delete();
    }

}