<?php
// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team Tx <admin@wkidt.com> 2018/5/12 11:32
// +----------------------------------------------------------------------
// | Readme: 管理员模型
// +----------------------------------------------------------------------


namespace  app\admin\model;

use think\Db;
use Wkidt\think5\model\Model;
use traits\model\SoftDelete;

class AdminModel extends Model
{
    use SoftDelete;

    /**
     * 时间自动完成
     *
     * @var bool
     */
    protected $autoWriteTimestamp = true;

    /**
     * 表名称
     *
     * @var string
     */
    protected $name = 'admin';


    protected  $encryption_str='wkidt';//加密字符

    /**
     * 管理员登录
     * @param $username
     * @param $password
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public  function  adminLogin($username,$password){
        if ('' == $username) {
            return ['code' => 'ERROR', 'info' =>  '请输入用户名'];
        }
        if ('' == $password) {
            return ['code' => 'ERROR', 'info' =>  '请输入密码'];
        }
        $admin=$this
               ->where(['username'=>$username,'password'=>Md5(Md5($password).$this->encryption_str)])
               ->field('password',true)
               ->find();

        if(!empty($admin)){
            session('admin_id',$admin['id']);
            session('admin_info',$admin);
            return ['code' => 'success', 'info' => '登录成功','data'=>$admin];
        }else{
            return ['code' => 'ERROR', 'info' =>  '登录失败'];
        }
    }



}