<?php
// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team Tx <admin@wkidt.com> 2018/5/12 11:32
// +----------------------------------------------------------------------
// | Readme: 管理员模型
// +----------------------------------------------------------------------


namespace  app\admin\model;

use think\Db;
use Wkidt\think5\model\Model;
use EasyWeChat\Foundation\Application;

class CouponesModel extends Model
{


    /**
     * 获取卡券名称
     * @return array|null
     * @throws \think\exception\DbException
     */
    public  function  getCardInfo($cardId){
        $app = new Application(config("EasyWechetConfig"));
        $card = $app->card;
        return $card->getCard($cardId);
    }


}