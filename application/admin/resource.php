<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/6/3 18:22
// +----------------------------------------------------------------------
// | Readme: 资源配置文件
// +----------------------------------------------------------------------

return [
    '后台资源' => [
        '后台登录' => [
            '管理员登录@adminLogin' => ['/login/login' => ['admin/Index/adminLogin', ['method' => 'post']],],
            '管理员退出@adminLogout' => ['/login/logout' => ['admin/Index/adminLogout', ['method' => 'get']],],
            '管理员信息@getAdminUserInfo' => ['/user/info' => ['admin/Index/getAdminUserInfo', ['method' => 'get']],]
            ],
        '首页控制台' => [
            '最新订单@admin_dashboard_order_list' => ['/dashboard/getOderList' => ['admin/Order/getOrderList', ['method' => 'post']],]
        ],

        '会员管理' => [
            '会员列表@admin_get_member_list' => ['/admin/getMemberList' => ['admin/Member/getMemberList', ['method' => 'get']],],
            '会员详情@admin_get_member_Info' => ['/admin/getMemberInfo' => ['admin/Member/getMemberInfo', ['method' => 'get']],],
            '会员详情@setMemberInfo' => ['/admin/setMemberInfo' => ['admin/Member/setMemberInfo', ['method' => 'post']],],
            '会员充值@addMoney' => ['/admin/addMoney' => ['admin/Order/addMoney', ['method' => 'post']],],
            '后台会员操作@MemberLog' => ['/admin/getLogList' => ['admin/Log/getLogList', ['method' => 'get']],]

        ],

        '订单' => [
            '支付订单列表@admin_get_order_list' => ['/admin/getOrderList' => ['admin/Order/getOrderList', ['method' => 'get']],],
            '商品订单列表@admingetOrderProductList' => ['/admin/getOrderProductList' => ['admin/Order/getOrderProductList', ['method' => 'get']],],
            '订单列表@admin_get_order_conut' => ['/admin/getOrderCount' => ['admin/Order/getOrderCount', ['method' => 'post']],],
            '订单详情@admin_get_order_info' => ['/admin/getOrderInfo' => ['admin/Order/getOrderInfo', ['method' => 'post']],],
            '提交订单@admin_add_order' => ['/admin/addOrder' => ['admin/Order/addOrder', ['method' => 'post']],]
        ],
        '产品' => [
            '产品列表@admin_get_product_list' => ['/admin/getProductList' => ['admin/Product/getProductList', ['method' => 'get']],],
            '添加产品@addProduct' => ['/admin/addProduct' => ['admin/Product/addProduct', ['method' => 'post']],],
            '更新产品@updateProduct' => ['/admin/updateProduct' => ['admin/Product/updateProduct', ['method' => 'post']],]
        ],

        '卡包' => [
            '卡券列表@gadmin_et_coupons_list' => ['/admin/getCouponsList' => ['admin/Coupons/getCouponsList', ['method' => 'post']],]
        ],

        '预约' => [
            '预约列表@admin_get_appointment_list' => ['/admin/get_member_appointment' => ['admin/Appointment/getMemberAppointment', ['method' => 'get']],],
            '取消用户预约@cancelAppointment' => ['/admin/cancelAppointment' => ['admin/Appointment/cancelAppointment', ['method' => 'post']],],
            '添加用户预约@admin_add_appointment' => ['/admin/add_member_appointment' => ['admin/Appointment/addMemberAppointment', ['method' => 'post']],]

        ],
        '预约管理' => [
            '获取预约时间@admin/admin_get_appointment_list' => ['admin/get_appointment_date' => ['admin/Appointment/getAppointmentDate', ['method' => 'get']],],
            '获取预约时间的点位@admin/getAppointmentDateDot' => ['admin/get_appointment_date_dot' => ['admin/Appointment/getAppointmentDateDot', ['method' => 'get']],],
            '添加预约项目@admin/addAppointmentProject' => ['admin/addAppointmentProject' => ['admin/Appointment/addAppointmentProject', ['method' => 'post']],],
            '修改预约项目@admin/setAppointmentProject' => ['admin/setAppointmentProject' => ['admin/Appointment/setAppointmentProject', ['method' => 'post']],],
            '删除预约项目@admin/deleteAppointmentProject' => ['admin/deleteAppointmentProject' => ['admin/Appointment/deleteAppointmentProject', ['method' => 'post']],],
            '修改预约次数@admin/setAppointmentNumber' => ['admin/set_appointment_number' => ['admin/Appointment/setAppointmentNumber', ['method' => 'post']],]
        ],
        '素材管理' => [
            '获取素材列表@admin/getMaterialList' => ['/getMaterialList' => ['admin/Material/getMaterialList', ['method' => 'get']],]
        ]
    ],

    '后台接口测试' =>[
        '后台登录' => [
            '管理员登录@adminoTest2' => ['/posttest' => ['admin/Index/test', ['method' => 'post']],],
            '管理员登录@adminTest' => ['/gettest' => ['admin/Index/test', ['method' => 'get']],]
            ]
        ]

];


