<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/24 0024 14:39
// +----------------------------------------------------------------------
// | Readme: 首页文件
// +----------------------------------------------------------------------

namespace app\admin\Controller;
use app\common\Controller\AdminBase;
use Wkidt\think5\request\Request;
use app\admin\model\ProductModel;

class Product extends AdminBase
{



    /**
     * 获取商品列表
     * @return mixed
     */
    public  function  getProductList(){
        $query = Request::instance()->get();
        return ProductModel::instance()->getProductList($query);
    }

    /**
     * 添加商品
     * @return mixed
     */
    public  function  addProduct(){
        $data = Request::instance()->post();
        return ProductModel::instance()->addProduct($data);
    }

    /**
     * 更新鲜商品
     * @return mixed
     */
    public  function  updateProduct(){
        $data = Request::instance()->post();
        return ProductModel::instance()->updateProduct($data);
    }



}