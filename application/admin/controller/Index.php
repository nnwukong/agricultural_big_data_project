<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team Tx <admin@wkidt.com> 2018/5/22 0024 14:39
// +----------------------------------------------------------------------
// | Readme: 管理员模块
// +----------------------------------------------------------------------

namespace app\admin\Controller;
use app\common\Controller\AdminBase;
use Wkidt\think5\request\Request;
use app\admin\model\AdminModel;
use app\admin\model\PhotoModel;
use EasyWeChat\Factory;
use EasyWeChat\Kernel\Messages\Text;
use EasyWeChat\Kernel\Messages\Transfer;

class Index extends AdminBase
{

    /**
     * 管理员登录
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function adminLogin()
    {
        $username = Request::instance()->post('username', '', 'trim');
        $password = Request::instance()->post('password', '', 'trim');
        return AdminModel::instance()->adminLogin($username,$password);
    }


    /**
     *管理员退出
     *
     * @return \think\response\json
     */
    public function adminLogout()
    {
        session('admin_id',null);
        session('admin_info',null);
        return ['code' => 'success', 'info' => '退出成功'];
    }

    /**
     * @return
     */
    public function test()
    {
        $app = Factory::officialAccount(config("EasyWechetConfig"));
        $rs = $app->template_message->getPrivateTemplates();

        $data['touser'] = 'ogeGT0uf2AmhgnEd5blUYf8b10Xs';
        $data['template_id'] = 'H5VHTabymIhMRuJ-mWs-CEUEi9KKHDfZH8YOsWer1uI';
        $data['url'] = 'http://oscar.wkidt.com/#/';
        $data['data']['first'] = '您卡号为586844661478的会员卡刚刚进行了消费';
        $data['data']['keyword1'] = '2018/08/29 17:30';
        $data['data']['keyword2'] = '商品:340,课程:380';
        $data['data']['keyword3'] = '商品:340,课程:380';
        $data['data']['keyword4'] = '商品:1340,课程:1280';
        $data['data']['remark'] = '奥斯卡亲子感谢您的支持';

        $app->template_message->send($data);
        return $rs;
    }

}