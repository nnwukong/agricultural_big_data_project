<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/24 0024 14:39
// +----------------------------------------------------------------------
// | Readme: 首页文件
// +----------------------------------------------------------------------

namespace app\admin\Controller;
use app\common\Controller\AdminBase;
use Wkidt\think5\request\Request;
use app\admin\model\PhotoModel;

class Photo extends AdminBase
{



    /**
     * 获取相片列表
     * @return mixed
     */
    public  function  getPhotoList(){
        return PhotoModel::instance()->getPhotoList();
    }

    /**
     * 获取相片详情
     */
    public function  getPhotoInfo(){
        $id=Request::instance()->get('id');
        return PhotoModel::instance()->getPhotoInfo($id);
    }


    /**
     * 设置照片基本信息
     * @return array
     */
    public  function  setPhotoBase(){
        $id=Request::instance()->post('id');
        $thumb=Request::instance()->post('thumb','','trim');
        $photo_src_zip=Request::instance()->post('photo_src_zip','','trim');
        $datetime=Request::instance()->post('datetime','','trim');
        return PhotoModel::instance()->setPhotoBase($id,$thumb,$photo_src_zip,$datetime);
    }


    /**
     * 上传照片底片
     * @return array
     */
    public  function  uploadingPhoto(){
        $id=Request::instance()->post('id');
        $photo_id=Request::instance()->post('photo_id');
        $photo_src_type=Request::instance()->post('photo_src_type','','trim');
        $photo_src=Request::instance()->post('photo_src','','trim');
        $isbuy=Request::instance()->post('isbuy','','trim');
        return PhotoModel::instance()->uploadingPhoto($id,$photo_id,$photo_src_type,$photo_src,$isbuy);
    }

    /**
     * 删除照片底片
     * @return array
     */
    public  function  delPhotoSrc(){
        $id=Request::instance()->post('id','','trim');
        return PhotoModel::instance()->delPhotoSrc($id);
    }


}