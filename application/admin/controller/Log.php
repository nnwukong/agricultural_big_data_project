<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/24 0024 14:39
// +----------------------------------------------------------------------
// | Readme: 日志控制器
// +----------------------------------------------------------------------

namespace app\admin\Controller;
use Wkidt\think5\request\Request;
use app\admin\model\LogModel;

class Log extends \Wkidt\think5\Controller
{
    /**
     * 日志列表
     * @param $username
     * @return object
     */
    public  function  getLogList(){
        //todo:需要优化自动带分页数据
        $query = Request::instance()->get();

        return LogModel::instance()->getLogList($query);
    }

    public  function  addMemberLog(){
        //todo:需要优化自动带分页数据
        $Log = Request::instance()->param();

        return LogModel::instance()->addMemberLog($Log);
    }
}