<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team Tx <admin@wkidt.com> 2016/10/24 0024 14:39
// +----------------------------------------------------------------------
// | Readme: 预约控制器
// +----------------------------------------------------------------------

namespace app\admin\Controller;
use app\common\Controller\AdminBase;
use think\Request;
use app\admin\model\AppointmentModel;
class Appointment extends AdminBase
{

    /**
     *  获取预约时间
     */
     public function  getAppointmentDate(){
         $shop_id=Request::instance()->get('shop_id','1','trim');
         return AppointmentModel::instance()->getAppointmentDate($shop_id);
     }

    /**
     * 获取预约项目表
     */
     public  function  getAppointmentDateDot(){
         $ad_id=Request::instance()->get('ad_id','0','trim');
         return AppointmentModel::instance()->getAppointmentDateDot($ad_id);
     }
    /**
     * 添加预约项目
     */
    public  function  addAppointmentProject(){
        $data=Request::instance()->post();
        return AppointmentModel::instance()->addAppointmentProject($data);
    }

    /**
     * 修噶预约项目
     */
    public  function  setAppointmentProject(){
        $data=Request::instance()->post();
        return AppointmentModel::instance()->setAppointmentProject($data);
    }

    /**
     * 删除预约项目
     */
    public  function  deleteAppointmentProject(){
        $data=Request::instance()->post("dot_id");
        return AppointmentModel::instance()->deleteAppointmentProject($data);
    }

    /**
     * 修改预约次数
     */
     public  function setAppointmentNumber(){
         $dot_id=Request::instance()->post('dot_id','0','trim');
         $appointment_sum=Request::instance()->post('appointment_sum','0','trim');
         return AppointmentModel::instance()->setAppointmentNumber($dot_id,$appointment_sum);
     }

    /**
     * 用户预约列表
     */
    public  function  getMemberAppointment(){
        $query = Request::instance()->get();
        return AppointmentModel::instance()->getMemberAppointment($query);
    }

    /**
     * 添加用户预约
     */
    public  function  addMemberAppointment(){
        $data['ad_id'] = Request::instance()->post('ad_id','0','trim');
        $data['dot_id'] = Request::instance()->post('dot_id','0','trim');
        $data['member_card'] = Request::instance()->post('member_card','0','trim');
        $data['project_id'] = Request::instance()->post('product_id','0','trim');
        return AppointmentModel::instance()->addMemberAppointment($data);
    }

    /**
     * 添加用户预约
     */
    public  function  cancelAppointment(){
        $ma_id= Request::instance()->post('ma_id','0','trim');
        return AppointmentModel::instance()->cancelAppointment($ma_id);
    }
}