<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team lockllb <admin@wkidt.com> 2016/10/24 0024 14:39
// +----------------------------------------------------------------------
// | 卡券控制器
// +----------------------------------------------------------------------

namespace app\admin\Controller;
use app\common\Controller\AdminBase;
class Coupons extends AdminBase
{

    /**
     * 获取用户领取的卡券列表
     *
     * @param string $openid 微信openID   如果不传人指定openID，则默认是所有的卡券
     * @return object json   返回卡券列表
     *
     */

    public function getCouponsList(){
        $app = new Application(config("EasyWechetConfig"));
        $card = $app->card;
        $offset      = 0;
        $count       = 50;

        //CARD_STATUS_NOT_VERIFY,待审核；
        //CARD_STATUS_VERIFY_FAIL,审核失败；
        //CARD_STATUS_VERIFY_OK，通过审核；
        //CARD_STATUS_USER_DELETE，卡券被商户删除；
        //CARD_STATUS_DISPATCH，在公众平台投放过的卡券；
        $statusList = 'CARD_STATUS_VERIFY_OK';

        $result = $card->lists($offset, $count, $statusList);
        $data['data'] = $result;

        return $data;
    }

    /**
     *  获取卡券详情
     *
     */
    public function getCouponsInfo($cardId){
        $app = new Application(config("EasyWechetConfig"));
        $card = $app->card;
        $cardInfo = json_decode($card->getCard($cardId));
        $data = $cardInfo->card->gift->base_info;
        return $data;
    }

}