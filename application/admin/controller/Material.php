<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/24 0024 14:39
// +----------------------------------------------------------------------
// | Readme: 素材控制器
// +----------------------------------------------------------------------

namespace app\admin\Controller;
use Wkidt\think5\request\Request;

class Material extends \Wkidt\think5\Controller
{

    /**
     * @return bool
     */
    public function getMaterialList ()
    {
        $app = new Application(config('EasyWechetConfig'));

        // 永久素材
        $material = $app->material;
        $lists = $material->lists('news');
        return ($lists);
    }
}