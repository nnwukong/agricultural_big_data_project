<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/24 0024 14:39
// +----------------------------------------------------------------------
// | Readme: 首页文件
// +----------------------------------------------------------------------

namespace app\admin\Controller;
use think\Db;
use Wkidt\think5\request\Request;
use app\common\Controller\AdminBase;
use app\admin\model\LogModel;
use EasyWeChat\Factory;

class Order extends AdminBase
{
    // 获取支付订单列表
    public function getOrderList()
    {
        $data=Request::instance()->get();
        $query['status'] = 1;
        if(isset($data['uid'])){
            $query['order.uid'] = $data['uid'];
        }
        $list = Db::name('order')
            ->alias('order')
            ->join('wk_member member','order.uid = member.uid','left')
            ->where($query)
            ->order('id desc')
            ->paginate();

        $data = $list->items();
        //var_dump($data);
        if ($data) {
            return ['data' => $data, 'page' => $list->getPageInfo()];
        } else {
            return null;
        }

    }

    // 获取详细订单列表
    public function getOrderProductList()
    {
        $data=Request::instance()->get();
        $query['order.status'] = 1;
        if(isset($data['uid'])){
            $query['order.uid'] = $data['uid'];
        }
        if(isset($data['type'])){
            $query['product_type'] = $data['type'];
        }
        $list = Db::name('order_product')
            ->alias('order_product')
            ->join('wk_order order','order.id = order_product.order_id','left')
            ->where($query)
            ->field('order.id,order.payType,
            order_product.product_id,
            order_product.title,
            order_product.product_type,
            order_product.product_class,
            order_product.num,
            order_product.count,
            order_product.addTime')
            ->order('order_product.order_id desc')
            ->paginate();

        $data = $list->items();
        //var_dump($data);
        if ($data) {
            return ['data' => $data, 'page' => $list->getPageInfo()];
        } else {
            return null;
        }

    }

    // 添加订单

    /**
     * @return array|null
     * @throws \EasyWeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function addOrder()
    {
        $data=Request::instance()->post();
        $data['status'] = 1;
        $data['addTime'] = time();
        $member= Db::name('member');

        // 扣除余额
        if($data['payType']==1){
            if($data['account_money']>0){
                $member->where('uid',$data['uid'])->setDec('member_account_money',$data['account_money']);
            }
            if($data['card_money']>0){
                $member->where('uid',$data['uid'])->setDec('member_card_money',$data['card_money']);
            }
        }
        // 添加支付订单记录
        $res = Db::name('order')->strict(true)->insertGetId($data);

        // 添加商品订单
        if($res){
            $product = json_decode($data['product'],true);
            foreach ($product as $item){
                $myproduct['order_id'] = $res;
                $myproduct['product_id'] = $item['id'];
                $myproduct['title'] = $item['title'];
                $myproduct['product_type'] = $item['type'];
                $myproduct['product_class'] = $item['class_id'];
                $myproduct['num'] = $item['num'];
                if (date("W")==0||date("W")==6){
                    $myproduct['count'] = $item['num']*$item['weekend_price'];
                }else{
                    $myproduct['count'] = $item['num']*$item['price'];
                }
                $myproduct['addTime'] = time();
                Db::name('order_product')->insert($myproduct);
            }
        }
        $memberInfo = $member->where('uid',$data['uid'])->find();
        // 推送模版消息
        $app = Factory::officialAccount(config("EasyWechetConfig"));
        $rs = $app->template_message->getPrivateTemplates();

        $sent_data['touser'] = $memberInfo['openid'];
        $sent_data['template_id'] = 'H5VHTabymIhMRuJ-mWs-CEUEi9KKHDfZH8YOsWer1uI';
        $sent_data['url'] = 'http://oscar.wkidt.com/#/';
        $sent_data['data']['first'] = "您卡号为".$memberInfo['member_card']."的会员卡刚刚进行了消费";
        $sent_data['data']['keyword1'] =date("Y-m-d H:i:s",time());
        $sent_data['data']['keyword2'] = '商品:'.($data['account_money']/100).',课程:'.($data['card_money']/100);
        $sent_data['data']['keyword3'] = '商品:'.($data['account_money']/100).',课程:'.($data['card_money']/100);
        $sent_data['data']['keyword4'] = '商品卡:'.($memberInfo['member_account_money']/100).',课程卡:'.($memberInfo['member_card_money']/100);
        $sent_data['data']['remark'] = '奥斯卡亲子感谢您的支持';

        $app->template_message->send($sent_data);
        if($res){
            return ['data' => $res];
        }else{
            return null;
        }

    }

    // 充值

    /**
     * @return bool
     * @throws \think\Exception
     */
    public function addMoney(){
        $data=Request::instance()->post();
        $data['money']=$data['money']*100;


        if($data['type']==1){
            $res = Db::name('member')->where('uid',$data['uid'])->setInc('member_account_money',$data['money']);
            $r =Db::name('account_log')->insert(['uid'=>$data['uid'],'type'=>$data['type'],'money'=>$data['money'],'mark'=>"admin",'updatetime'=>time()]);

            // 写日志
            $Log['title'] = "充值商品卡".$data['money']/100;
            $Log['uid'] = $data['uid'];
            LogModel::instance()->addMemberLog($Log);
        }
        if($data['type']==2){
            $res = Db::name('member')->where('uid',$data['uid'])->setInc('member_card_money',$data['money']);
            $r = Db::name('account_log')->insert(['uid'=>$data['uid'],'type'=>$data['type'],'money'=>$data['money'],'mark'=>"admin",'updatetime'=>time()]);

            // 写日志
            $Log['title'] = "充值课程卡".$data['money']/100;
            $Log['uid'] = $data['uid'];
            LogModel::instance()->addMemberLog($Log);
        }
        if($res) {

            return true;
        }else{
            return false;
        }

    }

}