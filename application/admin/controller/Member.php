<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/24 0024 14:39
// +----------------------------------------------------------------------
// | Readme: 首页文件
// +----------------------------------------------------------------------

namespace app\admin\Controller;
use app\common\Controller\AdminBase;
use Wkidt\think5\request\Request;
use app\admin\model\MemberModel;

class Member extends AdminBase
{


    /**
     * 获取会员列表
     * @return mixed
     */
    public  function  getMemberList(){
        return MemberModel::instance()->getMemberList();
    }


    /**
     * 获取会员列表
     * @return mixed
     */
    public  function  getMemberInfo(){
        $query = Request::instance()->get();

        return MemberModel::instance()->getMemberInfo($query);
    }

    /**
     * 更新会员信息
     * @return mixed
     */
    public  function  setMemberInfo(){
        $data = Request::instance()->post();

        return MemberModel::instance()->setMemberInfo($data);
    }
}