<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/24 0024 14:39
// +----------------------------------------------------------------------
// | Readme: 首页文件
// +----------------------------------------------------------------------

namespace app\admin\Controller;
use app\common\Controller\AdminBase;

class Dashboard extends AdminBase
{

    /**
     * 默认首页控制器
     */
    public function index(){

        return $this;

    }

}