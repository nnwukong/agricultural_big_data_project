<?php
// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team Tx <admin@wkidt.com> 2018/5/22 0024 14:39
// +----------------------------------------------------------------------
// | Readme: 管理员基础类
// +----------------------------------------------------------------------


namespace app\common\Controller;
class AdminBase extends \Wkidt\think5\Controller {


    //初始化验证模块
    protected function _initialize()
    {
        error_reporting(0);
        $passlist = array('adminlogin', 'adminLogout'); //不检测登陆状态的操作

        //把数组字符串转成小写
        $str=implode(',',$passlist);
        $lowercase=strtolower($str);
        $passlist=explode(',',$lowercase);
        $action =request()->action();
        $check = in_array($action, $passlist);
        if(!$check){
            if(empty(session('admin_id'))){
                echo json_encode( ['code' => 'LOGIN_ERROR', 'info' =>  '没有登录！']); die;
            }
        }

    }




}
