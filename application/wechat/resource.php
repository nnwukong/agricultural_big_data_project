<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/5/26 21:23
// +----------------------------------------------------------------------
// | Readme: 资源
// +----------------------------------------------------------------------

return [
    'easywechat' => [
        'test' =>['test@wechat/test' => ['test$' => ['wechat/Receive/test', ['method' => 'get']]]],
        'signature' =>['微信服务器配置@wx/index' => ['valid$' => ['wechat/Receive/index', ['method' => 'get']]]],
        'receive' =>['test@wechat/receive' => ['valid$' => ['wechat/Receive/index', ['method' => 'post']]]],
        'subscribe' =>['test@wechat/subscribe' => ['subscribe' => ['wechat/Receive/subscribe', ['method' => 'post']]]],
        'mytest' =>['test@wechat/mytest' => ['mytest' => ['wechat/Receive/test', ['method' => 'get']]]],
        'wx_oauth' =>['oauth@wechat/wx_oauth' => ['wx_oauth' => ['wechat/Index/wx_oauth', ['method' => 'get']]]],
        'oauth_callback' =>['oauth@wechat/oauth_callback' => ['oauth_callback' => ['wechat/Index/oauth_callback', ['method' => 'get']]]],
        'ok' =>['oauth@wechat/ok' => ['ok' => ['wechat/Index/ok', ['method' => 'get']]]],
        'WxPayNotify' =>['WxPayNotify' => ['WxPayNotify' => ['wechat/WxPayNotify/index', ['method' => 'post']]]],
        'jssdk' =>['jssdk' => ['jssdk' => ['wechat/Index/jssdk', ['method' => 'post']]]],
        'pay_jssdk' =>['pay_jssdk' => ['pay_jssdk' => ['wechat/Index/pay_jssdk', ['method' => 'post']]]],
        'getAccessToken' =>['getAccessToken' => ['getAccessToken' => ['wechat/Index/getAccessToken', ['method' => 'get']]]]
    ]

];



