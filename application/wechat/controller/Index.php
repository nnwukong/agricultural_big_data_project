<?php

namespace app\wechat\Controller;

use think\Session;
use think\Cookie;
use Wkidt\think5\request\Request;
use app\mobile\model\MemberModel;

class Index extends \Wkidt\think5\Controller{


    public function index(){

        $app = new Application(config("EasyWechetConfig"));

        $users = $app->user->lists();

        var_export($users);
    }

    /**
     * @return mixed
     */
    public function creatMenu(){
        $app = new Application(config("EasyWechetConfig"));
        /** @var TYPE_NAME $buttons */

        $buttons = array(

            array(
                "name"       => "小美好相馆",
                "sub_button" => array(
                    array(
                        "type" => "click",
                        "name" => "相馆在哪里",
                        "key"  => "where"),
                    array(
                        "type" => "click",
                        "name" => "最美证件照",
                        "key"  => "meimei"),
                    array(
                        "type" => "click",
                        "name" => "企业服务",
                        "key" => "company"),
                    array(
                        "type" => "click",
                        "name" => "加入小美好",
                        "key" => "hr")),),
            array(
                "type" => "click",
                "name" => "精彩活动",
                "key"  => "activity"),
            array(
                "name"       => "我的",
                "sub_button" => array(
                    array(
                        "type" => "view",
                        "name" => "相片",
                        "url"  => "http://admin.xmeihao.com/m/#/photo/my"),
                    array(
                        "type" => "view",
                        "name" => "订单",
                        "url"  => "http://admin.xmeihao.com/m/#/order/list"),
                    array(
                        "type" => "view",
                        "name" => "拍摄预约",
                        "url"  => "http://admin.xmeihao.com/m/#/appointment/index"),
                    array(
                        "type" => "view",
                        "name" => "个人中心",
                        "url"  => "http://admin.xmeihao.com/m/#/user/profile")),));
        //var_export(\GuzzleHttp\json_encode($buttons));
        $rs = $app->menu->add($buttons);
        return $rs ;
    }
    /*
     *
     * /*
         *
    {
        "button":[
         {
            "type":"view",
            "name":"课程预约",
            "url":"http://oscar.wkidt.com/#/appointment/index"

        },

        {
            "type":"view",
            "name":"奥斯卡动态",
            "url":"https://mp.weixin.qq.com/mp/homepage?__biz=MzUzMjkwNjg4MQ%3D%3D&hid=1&sn=5f3d509b8ab28e7a4f31f5ff0bfefc10"
        },
        {
            "type":"view",
            "name":"会员卡",
            "url":"http://oscar.wkidt.com/#/"

        }
        ]
    }

    {
            "type":"view",
            "name":"开业抽奖",
            "url":"http://xianchang.qq.com/live/client/index.html?campaign_id=928672"
        },
         *
         *
         *
         *
         */



    /**
     * @return bool
     */
    public  function wx_oauth(){
        $backurl=Request::instance()->get('backurl');
        //file_put_contents('log.txt', $backurl , FILE_APPEND);
        $app = new Application(config("EasyWechetConfig"));
        $oauth=$app->oauth;
        //获取判断
        if (empty(Session::get('wechat_user'))){
            Session::set('target_url', $backurl);
            $oauth->redirect()->send();
            return $oauth->redirect();
        }
        $rs['data']=Session::get('wechat_user');
        $openid = Cookie::get('openid');
        //var_export($user);
        //var_export($openid);
        //return $rs;
    }

    /**
     *
     */
    public function oauth_callback(){
        $app = new Application(config("EasyWechetConfig"));
        $oauth = $app->oauth;
        $user = $oauth->user()->toArray();
        //
        $member = new MemberModel();
        $rs = $member->getMemberInfoByOpenid($user['id']);

        if(!$rs){
            $data['openid'] = $user['original']['openid'];
            $data['nickname'] = $user['original']['nickname'];
            $data['sex'] = $user['original']['sex'];
            $data['city'] = $user['original']['city'];
            $data['province'] = $user['original']['province'];
            $data['country'] = $user['original']['country'];
            $data['headimgurl'] = $user['original']['headimgurl'];
            $member->addMember($data);
        }

        $openid =$user['id'];
        Session::set('wechat_user',$user);
        Cookie::set('openid',$user['id'],3600);
        Cookie::set('nickname',$user['nickname'],3600);
        Cookie::set('avatar',$user['avatar'],3600);
        $targetUrl =empty(Session::get('target_url'))?'/ok':Session::get('target_url');
        header('location:'. $targetUrl); // 跳转到 user/profile
    }

    /**
     *
     */
    public function ok(){
        $user=Session::get('wechat_user');
        return $user;
    }

    /**
     *
     */
    public function jssdk(){
        $app = new Application(config("EasyWechetConfig"));
        $APIs=array(
            'onMenuShareQQ', 'onMenuShareWeibo','onMenuShareTimeline','checkJsApi', 'openAddress','onMenuShareAppMessage'

        );
        $app->js->setUrl(Request::instance()->post('url'));
        $json = $app->js->config($APIs, $debug = false, $beta = false, $json = false);
        return $json;
    }

    /**
     *
     */
    public function pay_jssdk(){
        $app = new Application(config("EasyWechetConfig"));
        $payment = $app->payment;
        $configForPickAddress = $payment->configForShareAddress(Request::instance()->post('AccessToken'));
        return $configForPickAddress;
    }

    /**
     *
     */
    public function getAccessToken(){
        return getAccessToken();
    }

}