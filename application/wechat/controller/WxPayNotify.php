<?php

namespace app\wechat\Controller;

use EasyWeChat\Foundation\Application;
use think\Db;
use Wkidt\think5\request\Request;
use app\mobile\model\MemberModel;
use app\admin\model\PhotoModel;

class WxPayNotify extends \Wkidt\think5\Controller{
    //todo

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \EasyWeChat\Core\Exceptions\FaultException
     * $notify{
    "appid": "wx35d0a1b5485092c2",
    "attach": "test",
    "bank_type": "CFT",
    "cash_fee": "1",
    "fee_type": "CNY",
    "is_subscribe": "Y",
    "mch_id": "1340857801",
    "nonce_str": "0s63rd7mbyh6ufgps4e1si52a30ytwp4",
    "openid": "owZDKwc1KASexGvRmZrprDcIAWkM",
    "out_trade_no": "134085780120180601205740",
    "result_code": "SUCCESS",
    "return_code": "SUCCESS",
    "sign": "7591A3C672E70351DF0444AD979C6DE3",
    "time_end": "20180601205749",
    "total_fee": "1",
    "trade_type": "JSAPI",
    "transaction_id": "4200000113201806018444521400"
    }
    $successful{
    "appid": "wx35d0a1b5485092c2",
    "attach": "test",
    "bank_type": "CFT",
    "cash_fee": "1",
    "fee_type": "CNY",
    "is_subscribe": "Y",
    "mch_id": "1340857801",
    "nonce_str": "0s63rd7mbyh6ufgps4e1si52a30ytwp4",
    "openid": "owZDKwc1KASexGvRmZrprDcIAWkM",
    "out_trade_no": "134085780120180601205740",
    "result_code": "SUCCESS",
    "return_code": "SUCCESS",
    "sign": "7591A3C672E70351DF0444AD979C6DE3",
    "time_end": "20180601205749",
    "total_fee": "1",
    "trade_type": "JSAPI",
    "transaction_id": "4200000113201806018444521400"
    }
     *
     *
     */
    public function index()
    {
        $app = new Application(config("EasyWechetConfig"));
        $payment = $app->payment;
        $response = $app->payment->handleNotify(/**
         * @param $notify
         * @param $successful
         * @return bool|string
         */
            function($notify, $successful){
            file_put_contents('log.txt',$notify."\n\n",FILE_APPEND);
            file_put_contents('log.txt',$successful."\n\n",FILE_APPEND);
            // 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单

            $where['order_sn'] = $notify->out_trade_no;
            //$where['pay_status'] = 0;
            $order = Db::name('order')->where($where)->find();

            if (!$order) { // 如果订单不存在
                return 'Order not exist.'; // 告诉微信，我已经处理完了，订单没找到，别再通知我了
            }

            // 如果订单存在
            // 检查订单是否已经更新过支付状态
            if ($order['pay_status']!=0) { // 假设订单字段“支付时间”不为空代表已经支付
                return true; // 已经支付成功了就不再更新了
            }

            // 用户是否支付成功
            if ($successful) {
                // 不是已经支付状态则修改为已经支付状态
                $data['pay_status'] = 1 ;
                $data['pay_amount'] = $notify->total_fee;
                $data['pay_code'] = 'wxpay' ;
                $data['pay_time'] = time();
                
                //更新订单状态
                $rs = Db::name('order')->where('order_sn',$notify->out_trade_no)->update($data);


                // 如果是购买底片的,更新购买底片购买信息
                if(!empty($order['photo_src_id'] && (in_array($order['product_id'],[2,3,8,9])))){
                    Db::name('photo_src')->where('id',$order['photo_src_id'])->update(['isbuy'=>1]);
                }

                //如果是会员购买，更新会员信息
                if(!empty($order['openid'] && $order['product_id'] == 7)){
                    $where1['openid'] = $order['openid'];
                    Db::name('member')->where($where1)->update(['vip'=>1,'vip_addtime'=>time()]);
                }

                //更新相片底片购买状态
                //if(!empty($order_sn['photo_id'])){
                    //PhotoModel::instance()->updataSrcBuyStatus($order_sn['photo_id'],$order_sn['product_id']);
                //}else{
                    //return false;
                //}
            } else { // 用户支付失败
                //$data['pay_status'] = 3 ;
            }
            return true; // 返回处理完成
        });

        return $response;
    }
}