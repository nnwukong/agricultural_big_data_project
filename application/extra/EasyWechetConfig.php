<?php
return [
    /**
     * 账号基本信息，请从微信公众平台/开放平台获取
     */
    //'app_id'  => 'wx4ca6d06a268995d7',         // AppID
    //'secret'  => '20ff08838184c782ac0ed286b14aeb6d',     // AppSecret
    //'token'   => 'wkidt',          // Token
    //'aes_key' => '2MAwfypzXtcTdXwcRQNuw7syk6MAxpU3UQngNujRLaM',                    // EncodingAESKey，兼容与安全模式下请一定要填写！！！

    /**
     * 奥斯卡亲子之家微信公众平台
     */
    'app_id'  => 'wxbf4f242b0343f785',         // AppID
    'secret'  => '970ec71b2e48088647e33285540b7340',     // AppSecret
    'token'   => 'wkidt',          // Token
    'aes_key' => 'DlP8JLHBV6aQoRXblZa3ys8Ii2JEhyzLFayuu8SfAOX',                    // EncodingAESKey，兼容与安全模式下请一定要填写！！！

    /**
     * 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
     * 使用自定义类名时，构造函数将会接收一个 `EasyWeChat\Kernel\Http\Response` 实例
     */
    'response_type' => 'array',

    /**
     * 日志配置
     *
     * level: 日志级别, 可选为：
     *         debug/info/notice/warning/error/critical/alert/emergency
     * permission：日志文件权限(可选)，默认为null（若为null值,monolog会取0644）
     * file：日志文件位置(绝对路径!!!)，要求可写权限
     */
    'log' => [
        'level'      => 'debug',
        'permission' => 0777,
        'file' => __DIR__.'/wechat.log',
    ],

    /**
     * 接口请求相关配置，超时时间等，具体可用参数请参考：
     * http://docs.guzzlephp.org/en/stable/request-config.html
     *
     * - retries: 重试次数，默认 1，指定当 http 请求失败时重试的次数。
     * - retry_delay: 重试延迟间隔（单位：ms），默认 500
     * - log_template: 指定 HTTP 日志模板，请参考：https://github.com/guzzle/guzzle/blob/master/src/MessageFormatter.php
     */
    'http' => [
        'retries' => 1,
        'retry_delay' => 500,
        'timeout' => 5.0,
        // 'base_uri' => 'https://api.weixin.qq.com/', // 如果你在国外想要覆盖默认的 url 的时候才使用，根据不同的模块配置不同的 uri
    ],
    'payment' => [
        'merchant_id'        => '1340857801',
        'key'                => 'c327327c631829cb6964e9477b078e09',
        'cert_path'          => './cert/apiclient_cert.pem', // XXX: 绝对路径！！！！
        'key_path'           => './cert/apiclient_key.pem',      // XXX: 绝对路径！！！！
        'notify_url'         => 'http://admin.xmeihao.com/WxPayNotify',       // 你也可以在下单时单独设置来想覆盖它
        // 'device_info'     => '013467007045764',
        // 'sub_app_id'      => '',
        // 'sub_merchant_id' => '',
        // ...
    ],
    /**
     * OAuth 配置
     *
     * scopes：公众平台（snsapi_userinfo / snsapi_base），开放平台：snsapi_login
     * callback：OAuth授权完成后的回调页地址
     */
    'oauth' => [
        'scopes'   => ['snsapi_userinfo'],
        'callback' => '/oauth_callback',
    ],
];