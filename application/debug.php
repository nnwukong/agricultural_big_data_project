<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2017/5/28 15:28
// +----------------------------------------------------------------------
// | Readme: 调试环境配置
// +----------------------------------------------------------------------

return [

    /* 数据库配置 */
    'database' => [
        // 数据库类型
        'type'            => 'mysql',
        // 数据库连接DSN配置
        'dsn'             => '',
        // 服务器地址
        'hostname'        => '120.27.140.89',
        // 数据库名

        'database'        => 'gxstnu',

        // 数据库用户名
        'username'        => 'test',
        // 数据库密码
        'password'        => '1982Lock',
        // 数据库连接端口
        'hostport'        => 3306,
        // 数据库连接参数
        'params'          => [],
        // 数据库编码默认采用utf8
        'charset'         => 'utf8',
        // 数据库表前缀
        'prefix'          => 'wk_',
        // 数据库调试模式
        'debug'           => false,
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy'          => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate'     => false,
        // 读写分离后 主服务器数量
        'master_num'      => 1,
        // 指定从服务器序号
        'slave_no'        => '',
        // 是否严格检查字段是否存在
        'fields_strict'   => true,
        // 数据集返回类型
        'resultset_type'  => 'array',
        // 自动写入时间戳字段
        'auto_timestamp'  => false,
        // 时间字段取出后的默认时间格式
        'datetime_format' => 'Y-m-d H:i:s',
        // 是否需要进行SQL性能分析
        'sql_explain'     => false,
    ],


    // 上传配置


    'file' => [
        'type'              => 'oss',
        'access_key_id'     => 'LTAIOx8qb9f5ths8',  //悟空OSS
        'access_key_secret' => 'zphc9Cmx1rAA7nHYa8DzjKRht95xGv',//悟空OSS
        'endpoint'          => 'oss-cn-shenzhen.aliyuncs.com',
        'bucket'            => 'xiaomeihao',
        'role_arn'          => 'acs:ram::1493576833677829:role/xiaomeihao',
        'size'              => 5242880,
        'ext'               => 'jpg|jpeg|png|gif',
        'domain'            => 'xiaomeihao.oss-cn-shenzhen.aliyuncs.com',
    ],


    // 短信配置
    'sms_code' => [
        'type'          => 'Alidayu2',
        'app_key'       => 'LTAIKhRTzjOhzpSC',
        'app_secret'    => 'WIstW83d1CJffDxL37txtyZKGebvrX',
        'sign'          => '糖都农村电商',
        'template_code' => 'SMS_117526069',
        'valid_period'  => 30*60,
    ],

    // 图形验证
    'test' => [
        'type'          => 'CheckCode',
    ],
    //系统站内信有效期时间
    'sys_message_valid_for_time'=>30*24*60*60,  //30天
];
