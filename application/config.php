<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/22 0022 15:13
// +----------------------------------------------------------------------
// | Readme: 配置
// +----------------------------------------------------------------------

return [

    // 应用状态
    'app_status' => \think\Env::get('app_status', 'debug'),

    // 允许的后缀
    'url_html_suffix' => 'html|png|json|xml',

    // 路由配置
    'url_route_must' => true,

    // 默认返回类型
    'default_return_type' => 'json',


    // 扩展函数文件
    'extra_file_list'        => [THINK_PATH . 'helper' . EXT,APP_PATH.'common/Common/weixinfun.php'],

    // 默认主题
    'default_theme' => [
        'pc' => 'pc',
        'mobile' => 'mobile'
    ],

    // 异常处理类
    'exception_handle' => 'Wkidt\\think5\\exception\\Handle',

    // 跳转模板
    'dispatch_success_tmpl' => APP_PATH . 'system' . DS . 'tpl' . DS . 'dispatch_jump.tpl',
    'dispatch_error_tmpl' => APP_PATH . 'system' . DS . 'tpl' . DS . 'dispatch_jump.tpl',

    // 分页配置
    'paginate'               => [
        'type'      => 'Wkidt\\think5\\paginator\\driver\\Bootstrap',
        'var_page'  => 'page',
        'list_rows' => \think\Request::instance()->param('len', 18, 'intval') ?: 18,
    ],

    // 应用id
    'app_id' => 'client',

    // 资源配置文件列表
    'resource_file' => [
        VENDOR_PATH . "wkidt/file-manager/resource.php",
    ],

    // 语言包路径EasyWechetConfig
    'lang_dir' => [
        VENDOR_PATH . "wkidt/thinkphp5-extended/lang/",
    ],

    'cache'                  => [
        // 驱动方式
        'type'   => 'Wkidt\\think5\\cache\\driver\\File',
        // 缓存保存目录
        'path'   => CACHE_PATH,
        // 缓存前缀
        'prefix' => '',
        // 缓存有效期 0表示永久缓存
        'expire' => 0,
    ],
    //阿里运单查询
    'ali_logistics_host'=>'https://ali-deliver.showapi.com',  //请求域名
    'ali_logistics_path'=>'/showapi_expInfo', //请求方法
    'ali_logistics_method'=>'GET',  //请求方式
    'ali_logistics_appcode'=>'5214c126552e4710afd0c8f24eb04acd',  //APPcode

];
