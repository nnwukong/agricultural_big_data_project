<?php

// +----------------------------------------------------------------------
// | 悟空信息技术有限公司
// +----------------------------------------------------------------------
// | Copyright (c)2016 http://www.wkidt.com, All rights reserved.
// +----------------------------------------------------------------------
// | Author: wkidt team LSQ <admin@wkidt.com> 2016/10/24 0024 14:39
// +----------------------------------------------------------------------
// | Readme: 行为配置
// +----------------------------------------------------------------------

return [

    'app_init' => [
        \app\system\behavior\Resource::class,
        \app\system\behavior\Event::class,
        \app\system\behavior\Sys::class,
        \app\system\behavior\Lang::class,
    ],

    'app_begin' => [
        \app\system\behavior\Resource::class,   // 资源访问控制
        \app\system\behavior\Sys::class,        // 修改模板文件路径
        \app\system\behavior\Event::class
    ],

    'module_init' => [
        \app\system\behavior\Event::class
    ],

    'action_begin' => [
        \app\system\behavior\Event::class
    ],

    'view_filter' => [
        \app\system\behavior\Event::class
    ],

    'app_end' => [
        \app\system\behavior\Event::class
    ],

    'log_write' => [
        \app\system\behavior\Event::class
    ],

    'log_write_done' => [
        \app\system\behavior\Event::class
    ],

    'response_end' => [
        \app\system\behavior\Event::class
    ],

    'response_send' => [
        \app\system\behavior\Event::class
    ],

];
